import numpy as np
from scipy.integrate import simpson
from Utils import *
import torch


class Model:

    def __init__(self, do_standardise: bool):
        """
        Create the model
        """
        self.do_standardise = do_standardise
        self.X_mean = None
        self.y_mean = None
        self.X_std = None
        self.y_std = None
        pass

    def fit(self, X_train, y_train):

        """
        Fit the model to the training data.
        :param X_train: (ndarray) Observations
        :param y_train: (array) Function values
        :param standardise: (bool) To standardise or not to standardise
        :return:
        """
        raise NotImplementedError('implement fit function')

    def predict(self, X_test, y_test):
        """
        Use the fitted model to predict the values of test data
        :param X_test: (ndarray) Observations
        :param y_test: (array) Function values
        :return: (array) Predicted functin values, (float) prediction error
        """
        raise NotImplementedError('implement predict function')

    def standardise(self, X, y):
        """

        :param X:
        :param y:
        :return:
        """
        X = (X - self.X_mean) / (self.X_std + 1e-8)
        y = (y - self.y_mean) / (self.y_std + 1e-8)
        return X, y

    def unstandardise(self, X, y):
        """

        :param X:
        :param y:
        :return:
        """
        X = X * self.X_std + self.X_mean
        y = y * self.y_std + self.y_mean
        return X, y

    def update_standardisation_values(self, X, y):
        """

        :param X:
        :param y:
        :return:
        """
        self.X_mean = np.mean(X, axis=0)
        self.y_mean = np.mean(y, axis=0)
        self.X_std = np.std(X, axis=0)
        self.y_std = np.std(y, axis=0)

    def test_train_split(self, X_train, y_train, dim=0):
        # Find number of unique geometric values
        nunique = len(np.unique([X_train[:, dim]]))

        # Reshape data and pick out train and test
        X_test = X_train.reshape((nunique, X_train.shape[dim]//nunique, X_train.shape[-1]))[::2, :, :]
        X_train = X_train.reshape((nunique, X_train.shape[dim]//nunique, X_train.shape[-1]))[1::2, :, :]
        y_test = y_train.reshape((nunique, y_train.shape[dim] // nunique))[::2, :]
        y_train = y_train.reshape((nunique, y_train.shape[dim] // nunique))[1::2, :]

        # Reshape back
        X_train = X_train.reshape((-1, X_train.shape[2]))
        X_test = X_test.reshape((-1, X_test.shape[2]))
        y_train = y_train.flatten()
        y_test = y_test.flatten()

        return X_train, y_train, X_test, y_test

    def save_model(self, file: str):
        if file[-4:] != '.pth':
            file = file + '.pth'
        torch.save(self.model.state_dict(), file)



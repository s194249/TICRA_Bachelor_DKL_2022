from scipy.interpolate import interp1d, LinearNDInterpolator
import matplotlib.pyplot as plt
import numpy as np

def li(X, y, X_train, y_train, X_test, y_test):
    f = interp1d(X_train, y_train, fill_value='extrapolate')
    mean_prediction = f(X_test)
    err = np.mean(abs(y_test - mean_prediction)) # MAE / L1

    return mean_prediction, err
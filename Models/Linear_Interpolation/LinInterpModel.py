from Models.model import Model
from scipy.interpolate import interp1d, LinearNDInterpolator
import numpy as np

class LinInterpModel(Model):

    def __init__(self, do_standardise: bool = True):
        self.model = None
        self.name = 'LinInterp'
        Model.__init__(self, do_standardise)

    def fit(self, X_train, y_train, grid=None):

        if self.do_standardise:
            self.update_standardisation_values(X_train, y_train)
            X_train, y_train = self.standardise(X_train, y_train)

        if len(X_train.shape) == 1:
            self.model = interp1d(X_train, y_train, fill_value="extrapolate")
        else:
            fill_value = np.mean(y_train)
            self.model = LinearNDInterpolator(X_train, y_train, fill_value=fill_value)

    def predict(self, X_test, y_test):
        if self.model is None:
            raise ValueError('Model is not fitted yet')

        if self.do_standardise:
            X_test, _ = self.standardise(X_test, y_test)

        prediction = self.model(X_test)

        if self.do_standardise:
            X_test, prediction = self.unstandardise(X_test, prediction)

        return prediction

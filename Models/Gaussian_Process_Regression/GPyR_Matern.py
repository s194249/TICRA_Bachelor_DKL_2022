from Models.model import Model
import gpytorch
import torch
import tqdm
import numpy as np
seed = 11
np.random.seed(seed)
torch.manual_seed(seed)

class ExactGPModel(gpytorch.models.ExactGP):

    def __init__(self, X_train, y_train, likelihood):
        super(ExactGPModel, self).__init__(X_train, y_train, likelihood)
        X_dims = X_train.shape[1]
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.MaternKernel(nu=0.5, ard_num_dims=X_dims))

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)


class GPyRModelMatern(Model):

    def __init__(self,
                 likelihood: gpytorch.likelihoods.Likelihood,
                 training_iterations: int = 60,
                 do_standardise: bool = True):
        self.name = 'Matern'
        self.model = None
        self.likelihood = likelihood
        self.training_iterations = training_iterations
        Model.__init__(self, do_standardise)

    def fit(self, _X_train, _y_train,
            standardise: bool = True,
            verbose: bool = False,
            batch_size: int = 5_000,
            generalise: bool = False,
            grid=None,
            dim=0,
            bounds=None):

        if self.do_standardise:
            self.update_standardisation_values(_X_train, _y_train)
            _X_train, _y_train = self.standardise(_X_train, _y_train)

        if generalise:
            X_train, y_train, X_test, y_test = self.test_train_split(_X_train, _y_train, dim)
        else:
            X_train, y_train = _X_train, _y_train

        if len(X_train.shape) == 1:
            X_train = X_train.reshape(-1, 1)

        if generalise:
            # Convert to tensor
            _X_train = torch.tensor(_X_train, dtype=torch.float32)
            _y_train = torch.tensor(_y_train, dtype=torch.float32)
            X_train = torch.tensor(X_train, dtype=torch.float32)
            y_train = torch.tensor(y_train, dtype=torch.float32)
            X_test = torch.tensor(X_test, dtype=torch.float32)
            y_test = torch.tensor(y_test, dtype=torch.float32)
            if torch.cuda.is_available():
                _X_train = _X_train.cuda()
                _y_train = _y_train.cuda()
                X_train = X_train.cuda()
                y_train = y_train.cuda()
                X_test = X_test.cuda()
                y_test = y_test.cuda()
        else:
            X_train = torch.tensor(X_train, dtype=torch.float32)
            y_train = torch.tensor(y_train, dtype=torch.float32)
            if torch.cuda.is_available():
                X_train = X_train.cuda()
                y_train = y_train.cuda()
        # X_train = X_train.type(torch.DoubleTensor)
        # y_train = y_train.type(torch.DoubleTensor)

        self.model = ExactGPModel(X_train, y_train, self.likelihood)

        if torch.cuda.is_available():
            self.model = self.model.cuda()
            self.likelihood = self.likelihood.cuda()

        # Train the model
        # Find optimal model hyperparameters
        self.model.train()
        self.likelihood.train()

        optimizer = torch.optim.Adam(self.model.parameters(), lr=0.1)  # Includes GaussianLikelihood parameters

        # "Loss" for GPs - the marginal log likelihood
        mll = gpytorch.mlls.ExactMarginalLogLikelihood(self.likelihood, self.model)

        self.model.float()

        if generalise:
            dataset = torch.utils.data.TensorDataset(X_test, y_test)
        else:
            dataset = torch.utils.data.TensorDataset(X_train, y_train)
        dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size)
        dataset_size = len(dataloader.dataset)

        if verbose:
            for i in range(self.training_iterations):
                # Loop over batches in an epoch using DataLoader
                for id_batch, (x_batch, y_batch) in enumerate(dataloader):
                    # Zero gradients from previous iteration
                    optimizer.zero_grad()
                    # Output from model
                    self.model.set_train_data(x_batch, y_batch, strict=False)
                    y_batch_pred = self.model(x_batch)
                    # Calc loss and backprop gradients
                    loss = -mll(y_batch_pred, y_batch)
                    loss.backward()
                    optimizer.step()
                print(
                    f'Epoch: {i}  -  Loss: {loss.item():.4f}   Lengthscale: {self.model.covar_module.base_kernel.lengthscale.cpu().detach().numpy()[0]}  '
                    f'Outputscale: {self.model.covar_module.outputscale.item():.4f}')
                if torch.cuda.is_available():
                    torch.cuda.empty_cache()
        else:
            iterator = tqdm.tqdm(range(self.training_iterations))
            for i in iterator:
                # Loop over batches in an epoch using DataLoader
                for id_batch, (x_batch, y_batch) in enumerate(dataloader):
                    # Zero gradients from previous iteration
                    optimizer.zero_grad()
                    # Output from model
                    if generalise:
                        # with gpytorch.settings.debug(state=False):
                        #     y_batch_pred = self.model(x_batch)
                        self.model.eval()
                        self.likelihood.eval()
                        y_batch_pred = self.model(x_batch)
                        self.model.train()
                        self.likelihood.train()
                    else:
                        self.model.set_train_data(x_batch, y_batch, strict=False)
                        y_batch_pred = self.model(x_batch)
                    # Calc loss and backprop gradients
                    loss = -mll(y_batch_pred, y_batch)
                    loss.backward()
                    iterator.set_postfix(loss=loss.item(),
                                         outputscale=self.model.covar_module.outputscale.item(),
                                         lengthscales=self.model.covar_module.base_kernel.lengthscale.cpu().detach().numpy()[0],
                                         model=self.name)
                    optimizer.step()

        # set the entire data as train data before predicting on test data
        if generalise:
            self.model.set_train_data(_X_train, _y_train, strict=False)
        else:
            self.model.set_train_data(X_train, y_train, strict=False)

    def predict(self, X_test, y_test, return_std=False):

        if self.model is None:
            raise ValueError('Model is not fitted yet')

        if self.do_standardise:
            X_test, _ = self.standardise(X_test, y_test)

        if len(X_test.shape) == 1:
            X_test = X_test.reshape(-1, 1)

        # Set model mode to evaluation for faster predictions
        self.model.eval()
        self.likelihood.eval()

        with torch.no_grad(), gpytorch.settings.use_toeplitz(False), \
                gpytorch.settings.fast_pred_var(), gpytorch.beta_features.checkpoint_kernel(1000):
            X_test_tensor = torch.tensor(X_test, dtype=torch.float32)

            if torch.cuda.is_available():
                X_test_tensor = X_test_tensor.cuda()
            preds = self.model(X_test_tensor)

            # GIT line
            # predictive_distribution = likelihood(model(X_test))

        # restore original shape
        if X_test.shape[1] == 1:
            X_test = X_test.reshape(1, -1)[0]

        prediction = preds.mean.cpu().numpy()
        if self.do_standardise:
            X_test, prediction = self.unstandardise(X_test, prediction)

        if return_std:
            std = np.sqrt(preds.variance.detach().cpu().numpy())
            if self.do_standardise:
                std *= self.y_std
            return prediction, std

        return prediction

    def set_train_data(self, x_batch, y_batch, strict):
        pass

    def load_model(self, X_train, y_train, file: str):

        if self.do_standardise:
            self.update_standardisation_values(X_train, y_train)
            X_train, y_train = self.standardise(X_train, y_train)

        if len(X_train.shape) == 1:
            X_train = X_train.reshape(-1, 1)

            # Convert to tensor
        X_train = torch.tensor(X_train, dtype=torch.float32)
        y_train = torch.tensor(y_train, dtype=torch.float32)
        if torch.cuda.is_available():
            X_train = X_train.cuda()
            y_train = y_train.cuda()

        self.model = ExactGPModel(X_train, y_train, self.likelihood)
        try:
            if torch.cuda.is_available():
                state_dict = torch.load(file)
            else:
                state_dict = torch.load(file, map_location=torch.device('cpu'))
        except FileNotFoundError:
            raise FileNotFoundError('File not found')

        self.model.load_state_dict(state_dict)
        if torch.cuda.is_available():
            self.model.to(torch.device('cuda'))

if __name__ == '__main__':

    likelihood = gpytorch.likelihoods.GaussianLikelihood()
    likelihood.noise = 1e-4  # Some small value, but don't make it too small or numerical performance will suffer. I recommend 1e-4.
    likelihood.noise_covar.raw_noise.requires_grad_(False)
    model = GPyRModelMatern(likelihood=likelihood)

    import numpy as np
    import matplotlib.pyplot as plt
    func = lambda x: np.sin(x * 2) + 2

    X_train = np.arange(0, 8, 0.1)
    y_train = func(X_train)  #  X_train**2 / 2 + np.random.randn(len(X_train))*20 +

    # X_test = np.arange(8, 10, 0.1)
    # y_test = func(X_test)  # X_test**2 / 2 + np.random.randn(len(X_test))*20 + np.sin(X_test)*10

    model.fit(X_train, y_train, grid=100)

    X_test = np.arange(0, 10, 0.01)
    y_test = func(np.arange(0, 10, 0.01))
    mean, std = model.predict(X_test, y_test, return_std=True)

    f, ax = plt.subplots(1, 1, figsize=(4, 3), dpi=200)
    ax.plot(X_test, mean)
    ax.fill_between(X_test, mean - 2*std, mean + 2*std, alpha=0.5)
    ax.plot(X_test, y_test, linewidth=1)
    ax.plot(X_train, y_train, 'k*', markersize=1)
    plt.show()

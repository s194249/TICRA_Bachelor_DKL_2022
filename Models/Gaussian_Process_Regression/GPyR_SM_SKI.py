from Models.model import Model
import gpytorch
import torch
import tqdm
import numpy as np
from Utils import *
seed = 11
np.random.seed(seed)
torch.manual_seed(seed)

class SpectralMixtureGPModel(gpytorch.models.ExactGP):

    def __init__(self, X_train, y_train, likelihood, grid_size=100, bounds=None):
        super(SpectralMixtureGPModel, self).__init__(X_train, y_train, likelihood)
        X_dims = X_train.shape[1]
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.GridInterpolationKernel(
            gpytorch.kernels.SpectralMixtureKernel(ard_num_dims=X_dims, num_mixtures=4), num_dims=X_dims, grid_size=grid_size, grid_bounds=bounds)

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)


class GPyRModelSMSKI(Model):

    def __init__(self,
                 likelihood: gpytorch.likelihoods.Likelihood,
                 training_iterations: int = 60,
                 do_standardise: bool = True):
        self.name = 'SM_SKI'
        self.model = None
        self.likelihood = likelihood
        self.training_iterations = training_iterations
        Model.__init__(self, do_standardise)

    def fit(self, _X_train, _y_train,
            standardise: bool = True,
            verbose: bool = False,
            batch_size: int = 5_000,
            generalise: bool = False,
            grid=None,
            dim=0,
            bounds=None):

        if self.do_standardise:
            self.update_standardisation_values(_X_train, _y_train)
            _X_train, _y_train = self.standardise(_X_train, _y_train)

        if generalise:
            X_train, y_train, X_test, y_test = self.test_train_split(_X_train, _y_train, dim)
        else:
            X_train, y_train = _X_train, _y_train

        if len(X_train.shape) == 1:
            X_train = X_train.reshape(-1, 1)

        if generalise:
            # Convert to tensor
            _X_train = torch.tensor(_X_train, dtype=torch.float32)
            _y_train = torch.tensor(_y_train, dtype=torch.float32)
            X_train = torch.tensor(X_train, dtype=torch.float32)
            y_train = torch.tensor(y_train, dtype=torch.float32)
            X_test = torch.tensor(X_test, dtype=torch.float32)
            y_test = torch.tensor(y_test, dtype=torch.float32)
            if torch.cuda.is_available():
                _X_train = _X_train.cuda()
                _y_train = _y_train.cuda()
                X_train = X_train.cuda()
                y_train = y_train.cuda()
                X_test = X_test.cuda()
                y_test = y_test.cuda()
        else:
            X_train = torch.tensor(X_train, dtype=torch.float32)
            y_train = torch.tensor(y_train, dtype=torch.float32)
            if torch.cuda.is_available():
                X_train = X_train.cuda()
                y_train = y_train.cuda()
        # X_train = X_train.type(torch.DoubleTensor)
        # y_train = y_train.type(torch.DoubleTensor)
        if bounds is not None:
            X, _ = self.standardise(bounds.T, 0)
            X = X.T
            if len(X.shape) == 1:
                X = X.reshape(1, -1)
            bounds = ()
            for row in X:
                a, b = row
                bounds += ((a, b), )

        self.model = SpectralMixtureGPModel(X_train, y_train, self.likelihood, grid_size=grid, bounds=bounds)

        if torch.cuda.is_available():
            self.model = self.model.cuda()
            self.likelihood = self.likelihood.cuda()

        # Train the model
        # Find optimal model hyperparameters
        self.model.train()
        self.likelihood.train()

        optimizer = torch.optim.Adam(self.model.parameters(), lr=0.1)  # Includes GaussianLikelihood parameters

        # "Loss" for GPs - the marginal log likelihood
        mll = gpytorch.mlls.ExactMarginalLogLikelihood(self.likelihood, self.model)

        self.model.float()

        if verbose:
            for i in range(self.training_iterations):
                # Zero backprop gradients
                optimizer.zero_grad()
                # Get output from model
                if generalise:
                    output = self.model(X_test)
                    loss = -mll(output, y_test)
                else:
                    output = self.model(X_train)
                    loss = -mll(output, y_train)
                # Calc loss and backprop derivatives
                loss.backward()
                optimizer.step()
                print(
                    f'Epoch: {i}  -  Loss: {loss.item():.4f}   Lengthscale: {self.model.covar_module.base_kernel.base_kernel.lengthscale.cpu().detach().numpy()[0]}  '
                    f'Outputscale: {self.model.covar_module.outputscale.item():.4f}')
                if torch.cuda.is_available():
                    torch.cuda.empty_cache()
        else:
            iterator = tqdm.tqdm(range(self.training_iterations))
            for i in iterator:
                # Zero gradients from previous iteration
                optimizer.zero_grad()
                # Output from model
                if generalise:
                    with gpytorch.settings.debug(state=False):
                        output = self.model(X_test)
                    # Calc loss and backprop gradients
                    loss = -mll(output, y_test)
                else:
                    output = self.model(X_train)
                    loss = -mll(output, y_train)
                loss.backward()
                iterator.set_postfix(loss=loss.item(),
                                         mixture_weights=self.model.covar_module.base_kernel.mixture_weights.cpu().detach().numpy(),
                                         model=self.name)
                optimizer.step()

        if generalise:
            # set the entire data as train data before predicting on test data
            self.model.set_train_data(_X_train, _y_train, strict=False)
        else:
            self.model.set_train_data(X_train, y_train, strict=False)

    def predict(self, X_test, y_test, return_std=False):

        if self.model is None:
            raise ValueError('Model is not fitted yet')

        if self.do_standardise:
            X_test, _ = self.standardise(X_test, y_test)

        if len(X_test.shape) == 1:
            X_test = X_test.reshape(-1, 1)

        # Set model mode to evaluation for faster predictions
        self.model.eval()
        self.likelihood.eval()

        with torch.no_grad(), gpytorch.settings.fast_pred_var(), gpytorch.beta_features.checkpoint_kernel(1000):  #
            # If cuda is available, don't use toeplitz
            if torch.cuda.is_available():
                with gpytorch.settings.use_toeplitz(False):
                    X_test_tensor = torch.tensor(X_test, dtype=torch.float32)

                    if torch.cuda.is_available():
                        X_test_tensor = X_test_tensor.cuda()
                    preds = self.model(X_test_tensor)
            else:
                with gpytorch.settings.use_toeplitz(True):
                    X_test_tensor = torch.tensor(X_test, dtype=torch.float32)

                    if torch.cuda.is_available():
                        X_test_tensor = X_test_tensor.cuda()
                    preds = self.model(X_test_tensor)

            # GIT line
            # predictive_distribution = likelihood(model(X_test))

        # restore original shape
        if X_test.shape[1] == 1:
            X_test = X_test.reshape(1, -1)[0]

        prediction = preds.mean.cpu().numpy()
        if self.do_standardise:
            X_test, prediction = self.unstandardise(X_test, prediction)

        if return_std:
            std = np.sqrt(preds.variance.detach().cpu().numpy())
            if self.do_standardise:
                std *= self.y_std
            return prediction, std

        return prediction

    def set_train_data(self, x_batch, y_batch, strict):
        pass

    def load_model(self, X_train, y_train, bounds, file: str):

        if self.do_standardise:
            self.update_standardisation_values(X_train, y_train)
            X_train, y_train = self.standardise(X_train, y_train)

        if len(X_train.shape) == 1:
            X_train = X_train.reshape(-1, 1)

            # Convert to tensor
        X_train = torch.tensor(X_train, dtype=torch.float32)
        y_train = torch.tensor(y_train, dtype=torch.float32)
        if torch.cuda.is_available():
            X_train = X_train.cuda()
            y_train = y_train.cuda()

        if bounds is not None:
            X, _ = self.standardise(bounds, 0)
            if len(X.shape) == 1:
                X = X.reshape(1, -1)
            bounds = ()
            for row in X:
                a, b = row
                bounds += ((a, b), )

        try:
            if torch.cuda.is_available():
                state_dict = torch.load(file)
            else:
                state_dict = torch.load(file, map_location=torch.device('cpu'))
        except FileNotFoundError:
            raise FileNotFoundError('File not found')
        if state_dict.get('covar_module.grid_0') is not None:
            self.model = SpectralMixtureGPModel(X_train, y_train, self.likelihood, grid_size=len(state_dict['covar_module.grid_0']), bounds=bounds)
        else:
            self.model = SpectralMixtureGPModel(X_train, y_train, self.likelihood, grid_size=len(state_dict['covar_module.base_kernel.grid_0']), bounds=bounds)
        self.model.load_state_dict(state_dict)
        if torch.cuda.is_available():
            self.model.to(torch.device('cuda'))

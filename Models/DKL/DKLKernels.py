import gpytorch
import torch


class LargeFeatureExtractor(torch.nn.Sequential):
    def __init__(self, data_dim, out_dim=None):
        super(LargeFeatureExtractor, self).__init__()
        self.out_dim = out_dim if out_dim is not None else data_dim
        self.add_module('linear1', torch.nn.Linear(data_dim, 1000))
        self.add_module('relu1', torch.nn.ReLU())
        self.add_module('linear2', torch.nn.Linear(1000, 500))
        self.add_module('relu2', torch.nn.ReLU())
        self.add_module('linear3', torch.nn.Linear(500, 50))
        self.add_module('relu3', torch.nn.ReLU())
        self.add_module('linear4', torch.nn.Linear(50, self.out_dim))


class SpectralMixtureGPModel(gpytorch.models.ExactGP):
    name = 'SM'

    def __init__(self, X_train, y_train, likelihood, feature_extractor, grid=100):
        super(SpectralMixtureGPModel, self).__init__(X_train, y_train, likelihood)
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.GridInterpolationKernel(
            gpytorch.kernels.SpectralMixtureKernel(ard_num_dims=feature_extractor.out_dim, num_mixtures=4),
            num_dims=feature_extractor.out_dim, grid_size=grid)

        self.feature_extractor = feature_extractor

        # This module will scale the NN features so that they're nice values
        self.scale_to_bounds = gpytorch.utils.grid.ScaleToBounds(-1., 1.)

    def forward(self, x):
        # We're first putting our data through a deep net (feature extractor)
        projected_x = self.feature_extractor(x)
        projected_x = self.scale_to_bounds(projected_x)  # Make the NN values "nice"

        mean_x = self.mean_module(projected_x)
        covar_x = self.covar_module(projected_x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)


class RBFGPModel(gpytorch.models.ExactGP):
    name = 'RBF'

    def __init__(self, X_train, y_train, likelihood, feature_extractor, grid=100):
        super(RBFGPModel, self).__init__(X_train, y_train, likelihood)
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.GridInterpolationKernel(
            gpytorch.kernels.RBFKernel(ard_num_dims=feature_extractor.out_dim), num_dims=feature_extractor.out_dim, grid_size=grid))

        self.feature_extractor = feature_extractor

        # This module will scale the NN features so that they're nice values
        self.scale_to_bounds = gpytorch.utils.grid.ScaleToBounds(-1., 1.)

    def forward(self, x):
        # We're first putting our data through a deep net (feature extractor)
        projected_x = self.feature_extractor(x)
        projected_x = self.scale_to_bounds(projected_x)  # Make the NN values "nice"

        mean_x = self.mean_module(projected_x)
        covar_x = self.covar_module(projected_x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)


class MaternGPModel(gpytorch.models.ExactGP):
    name = 'Matern'

    def __init__(self, X_train, y_train, likelihood, feature_extractor, grid=100):
        super(MaternGPModel, self).__init__(X_train, y_train, likelihood)
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.GridInterpolationKernel(
            gpytorch.kernels.MaternKernel(nu=0.5, ard_num_dims=feature_extractor.out_dim), num_dims=feature_extractor.out_dim,
            grid_size=grid))

        self.feature_extractor = feature_extractor

        # This module will scale the NN features so that they're nice values
        self.scale_to_bounds = gpytorch.utils.grid.ScaleToBounds(-1., 1.)

    def forward(self, x):
        # We're first putting our data through a deep net (feature extractor)
        projected_x = self.feature_extractor(x)
        projected_x = self.scale_to_bounds(projected_x)  # Make the NN values "nice"

        mean_x = self.mean_module(projected_x)
        covar_x = self.covar_module(projected_x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)


class MaternGPModelNOSKI(gpytorch.models.ExactGP):
    name = 'Matern_NO_SKI'

    def __init__(self, X_train, y_train, likelihood, feature_extractor, grid=100):
        super(MaternGPModelNOSKI, self).__init__(X_train, y_train, likelihood)
        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(
            gpytorch.kernels.MaternKernel(nu=0.5, ard_num_dims=feature_extractor.out_dim))

        self.feature_extractor = feature_extractor

        # This module will scale the NN features so that they're nice values
        self.scale_to_bounds = gpytorch.utils.grid.ScaleToBounds(-1., 1.)

    def forward(self, x):
        # We're first putting our data through a deep net (feature extractor)
        projected_x = self.feature_extractor(x)
        projected_x = self.scale_to_bounds(projected_x)  # Make the NN values "nice"

        mean_x = self.mean_module(projected_x)
        covar_x = self.covar_module(projected_x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)
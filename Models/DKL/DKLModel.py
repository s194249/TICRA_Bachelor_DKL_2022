from Models.model import Model
from Models.DKL.DKLKernels import SpectralMixtureGPModel, LargeFeatureExtractor
import gpytorch
import torch
import tqdm
import numpy as np

class DKLModel(Model):

    def __init__(self,
                 likelihood: gpytorch.likelihoods.Likelihood,
                 feature_extractor,
                 GP_model = SpectralMixtureGPModel,
                 training_iterations: int = 60,
                 do_standardise: bool = True,):
        self.name = 'DKL_' + GP_model.name
        self.model = None
        self.likelihood = likelihood
        self.feature_extractor = feature_extractor
        self.GP_model = GP_model
        self.training_iterations = training_iterations
        Model.__init__(self, do_standardise)

    def initialize_model(self, X_train, y_train, grid):
        return self.GP_model(X_train, y_train, self.likelihood, self.feature_extractor(X_train.size(-1)), grid=grid)

    def fit(self, _X_train, _y_train,
            standardise: bool = True,
            verbose: bool = False,
            batch_size: int = 5_000,
            generalise: bool = False,
            grid=None,
            dim=0,
            bounds=None):

        if self.do_standardise:
            self.update_standardisation_values(_X_train, _y_train)
            _X_train, _y_train = self.standardise(_X_train, _y_train)

        if generalise:
            X_train, y_train, X_test, y_test = self.test_train_split(_X_train, _y_train, dim)
        else:
            X_train, y_train = _X_train, _y_train

        if len(X_train.shape) == 1:
            X_train = X_train.reshape(-1, 1)

        if generalise:
            # Convert to tensor
            _X_train = torch.tensor(_X_train, dtype=torch.float32)
            _y_train = torch.tensor(_y_train, dtype=torch.float32)
            X_train = torch.tensor(X_train, dtype=torch.float32)
            y_train = torch.tensor(y_train, dtype=torch.float32)
            X_test = torch.tensor(X_test, dtype=torch.float32)
            y_test = torch.tensor(y_test, dtype=torch.float32)
            if torch.cuda.is_available():
                _X_train = _X_train.cuda()
                _y_train = _y_train.cuda()
                X_train = X_train.cuda()
                y_train = y_train.cuda()
                X_test = X_test.cuda()
                y_test = y_test.cuda()
        else:
            X_train = torch.tensor(X_train, dtype=torch.float32)
            y_train = torch.tensor(y_train, dtype=torch.float32)
            if torch.cuda.is_available():
                X_train = X_train.cuda()
                y_train = y_train.cuda()

        self.model = self.initialize_model(X_train, y_train, grid)

        if torch.cuda.is_available():
            self.model = self.model.cuda()
            self.likelihood = self.likelihood.cuda()

        # Train the model
        # Find optimal model hyperparameters
        self.model.train()
        self.likelihood.train()

        optimizer = torch.optim.Adam([
            {'params': self.model.feature_extractor.parameters()},
            {'params': self.model.covar_module.parameters()},
            {'params': self.model.mean_module.parameters()},
            {'params': self.model.likelihood.parameters()},
        ], lr=0.01)

        # "Loss" for GPs - the marginal log likelihood
        mll = gpytorch.mlls.ExactMarginalLogLikelihood(self.likelihood, self.model)
        losses = np.zeros(self.training_iterations)
        if verbose:
            for i in range(self.training_iterations):
                # Zero backprop gradients
                optimizer.zero_grad()
                # Get output from model
                if generalise:
                    output = self.model(X_test)
                    loss = -mll(output, y_test)
                else:
                    output = self.model(X_train)
                    loss = -mll(output, y_train)
                losses[i] = loss.item()
                # Calc loss and backprop derivatives
                loss.backward()
                optimizer.step()
                print(
                    f'Epoch: {i}  -  Loss: {loss.item():.4f}   Lengthscale: {self.model.covar_module.base_kernel.base_kernel.lengthscale.cpu().detach().numpy()[0]}  '
                    f'Outputscale: {self.model.covar_module.outputscale.item():.4f}')
                if torch.cuda.is_available():
                    torch.cuda.empty_cache()
        else:
            iterator = tqdm.tqdm(range(self.training_iterations))
            for i in iterator:
                # Zero gradients from previous iteration
                optimizer.zero_grad()
                if generalise:
                    # Output from model
                    output = self.model(X_test)
                    # Calc loss and backprop gradients
                    loss = -mll(output, y_test)
                else:
                    output = self.model(X_train)
                    loss = -mll(output, y_train)
                losses[i] = loss.item()
                loss.backward()
                if self.name == 'DKL_SM':
                    iterator.set_postfix(loss=loss.item(),
                                         model=self.name,
                                         mixture_scales=[round(x, 2) for x in (self.model.covar_module.base_kernel.mixture_scales.cpu().detach().numpy()).flatten()])
                else:
                    iterator.set_postfix(loss=loss.item(),
                                         model=self.name,
                                         outputscale=self.model.covar_module.outputscale.item(),
                                         lengthscales=self.model.covar_module.base_kernel.base_kernel.lengthscale.cpu().detach().numpy()[0])
                optimizer.step()

        # set the entire data as train data before predicting on test data
        if generalise:
            self.model.set_train_data(_X_train, _y_train, strict=False)
        return losses

    def predict(self, X_test, y_test, return_std=False):

        if self.model is None:
            raise ValueError('Model is not fitted yet')

        if self.do_standardise:
            X_test, _ = self.standardise(X_test, y_test)

        if len(X_test.shape) == 1:
            X_test = X_test.reshape(-1, 1)

        # Set model mode to evaluation for faster predictions
        self.model.eval()
        self.likelihood.eval()

        with torch.no_grad(), gpytorch.settings.use_toeplitz(False), gpytorch.settings.fast_pred_var():
            X_test_tensor = torch.tensor(X_test, dtype=torch.float32)
            if torch.cuda.is_available():
                X_test_tensor = X_test_tensor.cuda()
            preds = self.model(X_test_tensor)

        # restore original shape
        if X_test.shape[1] == 1:
            X_test = X_test.reshape(1, -1)[0]

        prediction = preds.mean.cpu().numpy()
        if self.do_standardise:
            X_test, prediction = self.unstandardise(X_test, prediction)

        if return_std:
            std = np.sqrt(preds.variance.cpu().numpy())
            if self.do_standardise:
                std *= self.y_std
            return prediction, std

        return prediction

    def set_training_data(self, X_train, y_train, strict: bool = False):

        if self.do_standardise:
            #self.update_standardisation_values(X_train, y_train)
            X_train, y_train = self.standardise(X_train, y_train)

        if len(X_train.shape) == 1:
            X_train = X_train.reshape(-1, 1)

        # Convert to tensor
        X_train = torch.tensor(X_train, dtype=torch.float32)
        y_train = torch.tensor(y_train, dtype=torch.float32)
        if torch.cuda.is_available():
            X_train = X_train.cuda()
            y_train = y_train.cuda()

        self.model.set_train_data(X_train, y_train, strict=strict)

    def fantasy_model(self, X_sample, y_sample, **kwargs):
        if self.do_standardise:
            X_sample, y_sample = self.standardise(X_sample, y_sample)

        if len(X_sample.shape) == 1:
            X_sample = X_train.reshape(-1, 1)

        # Convert to tensor
        X_sample = torch.tensor(X_sample, dtype=torch.float32)
        y_sample = torch.tensor(y_sample, dtype=torch.float32)
        if torch.cuda.is_available():
            X_sample = X_sample.cuda()
            y_sample = y_sample.cuda()

        self.model = self.model.get_fantasy_model(X_sample, y_sample, **kwargs)

    def load_model(self, X_train, y_train, file: str):

        if self.do_standardise:
            self.update_standardisation_values(X_train, y_train)
            X_train, y_train = self.standardise(X_train, y_train)

        if len(X_train.shape) == 1:
            X_train = X_train.reshape(-1, 1)

            # Convert to tensor
        X_train = torch.tensor(X_train, dtype=torch.float32)
        y_train = torch.tensor(y_train, dtype=torch.float32)
        if torch.cuda.is_available():
            X_train = X_train.cuda()
            y_train = y_train.cuda()

        try:
            if torch.cuda.is_available():
                state_dict = torch.load(file)
            else:
                state_dict = torch.load(file, map_location=torch.device('cpu'))
        except FileNotFoundError:
            raise FileNotFoundError('File not found')
        if state_dict.get('covar_module.grid_0') is not None:
            self.model = self.initialize_model(X_train, y_train, grid=len(state_dict['covar_module.grid_0']))
        else:
            self.model = self.initialize_model(X_train, y_train, grid=len(state_dict['covar_module.base_kernel.grid_0']))
        self.model.load_state_dict(state_dict)
        if torch.cuda.is_available():
            self.model.to(torch.device('cuda'))


if __name__ == '__main__':
    feature_extractor = LargeFeatureExtractor

    likelihood = gpytorch.likelihoods.GaussianLikelihood()
    likelihood.noise = 1e-4  # Some small value, but don't make it too small or numerical performance will suffer. I recommend 1e-4.
    likelihood.noise_covar.raw_noise.requires_grad_(False)
    model = DKLModel(likelihood=likelihood, feature_extractor=feature_extractor)

    import numpy as np
    import matplotlib.pyplot as plt

    X_train = np.arange(5, 100, 10)
    y_train = X_train**2 + np.random.randn(len(X_train))*200

    X_test = np.arange(1, 100)
    y_test = X_test**2 + np.random.randn(len(X_test))*200

    model.fit(X_train, y_train)

    mean, std = model.predict(X_test, y_test, return_std=True)

    f, ax = plt.subplots(1, 1, figsize=(4, 3), dpi=200)
    ax.plot(X_test, mean)
    ax.fill_between(X_test, mean - 2*std, mean + 2*std, alpha=0.5)
    ax.plot(X_test, y_test)
    ax.plot(X_train, y_train, 'k*')
    plt.show()

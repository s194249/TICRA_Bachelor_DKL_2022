import numpy as np

# Preprocess stuff should be removed
def preprocess(X_train, y_train, X_test, y_test):
    # Mean removal and variance scaling using training data
    X_test = (X_test - np.mean(X_train, axis=0)) / np.std(X_train, axis=0)
    y_test = (y_test - np.mean(y_train, axis=0)) / np.std(y_train, axis=0)
    X_train = (X_train - np.mean(X_train, axis=0)) / np.std(X_train, axis=0)
    y_train = (y_train - np.mean(y_train, axis=0)) / np.std(y_train, axis=0)

    return X_train, y_train, X_test, y_test


class DataSplit:
    def __init__(self, X, y, training_fraction: float = 0.05, max_folds: int = 10):
        self.X = X
        self.y = y
        self.training_fraction = training_fraction
        self.step = int(1 / training_fraction)  # step size between each data point
        self.folds = min(self.step, max_folds)  # amount of K folds used

    def data_split(self, offset):
        # Take every int(1 / training_fraction) data points for training starting at offset
        X_train = self.X[offset::self.step]
        y_train = self.y[offset::self.step]

        # Remove every int(1 / training_fraction) data points from testing
        X_test = np.delete(self.X, np.arange(offset, self.X.shape[0], self.step), axis=0)
        y_test = np.delete(self.y, np.arange(offset, self.y.shape[0], self.step), axis=0)

        return X_train, y_train, X_test, y_test


    def get_splits(self):
        # Create step size for the offset depending on the step size and number of folds
        offset_step = self.step / self.folds
        train_test_splits = []

        # Split data by offsetting starting point each time
        for k in range(self.folds):
            offset = int(k * offset_step)
            train_test_splits += [self.data_split(offset)]

        return train_test_splits


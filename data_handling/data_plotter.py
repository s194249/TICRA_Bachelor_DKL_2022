import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import pandas as pd
import os
import matplotlib.font_manager as fm
from data_handling.data_structure import Data


class Plotter:

    def __init__(self, data):
        self.data = data
        self.units = {'Mag': 'Magnitude [dB]', 'Phase': 'Phase [deg]'}

    def plot2d(self, rc: (int, int), type='Mag', geom_indices: [int] = [0], **kwargs):
        fig, ax = plt.subplots()
        S, freq, geom = self.data._S(rc, type, geom_indices)
        S, freq = S[::20], freq[::20]
        ax.plot(freq, S, '-', c='#626567', markersize=2, linewidth=0.7, label=str(round(geom[0], 2)), zorder=1)
        ax.scatter(freq, S, c=S, cmap='RdBu', s=10, label=str(round(geom[0], 2)), zorder=2)  # edgecolors='black', linewidth=0.01,
        # ax.set_title(f'Magnitude for geometric value {str(round(geom[0], 2))}')
        ax.set_xlabel('Frequency [GHz]')
        ax.set_ylabel(self.units[type])
        # ax.legend(title='Geometric Value', loc='lower right')
        ax.grid(alpha=0.25, linestyle='dashed')
        plt.tight_layout()
        # plt.savefig(f'../2d/results/S(1,1)/figures/dielectric_no_outline.pdf')
        plt.show()

    def plot3d(self, rc: (int, int), type='Mag', geom_indices: [int] = [None]):
        S, freq, geom = self.data._S(rc, type, geom_indices)
        geom = [g for i, g in zip(geom_indices, geom) if i == None][0]
        freq = np.repeat(freq[:, np.newaxis], geom.shape[0], axis=1).T
        geom = np.repeat(geom[:, np.newaxis], freq.shape[1], axis=1)
        S = S.reshape((geom.shape[0], geom.shape[1]))

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        surf = ax.plot_surface(freq, geom, S, cmap=cm.coolwarm_r)
        ax.set_title(self.data.format_input(rc, type))
        ax.set_ylabel('Geom. Param.')
        ax.set_xlabel('Frequency [GHz]')
        ax.set_zlabel(self.units[type])
        fig.colorbar(surf, shrink=0.5, aspect=5)
        ax.view_init(elev=55, azim=-140)
        plt.show()

    def plot3dlines(self, rc: (int, int)):
        S, freq, geom = self.data._S(rc, type='Mag')
        geom2 = geom[0]
        S2 = S.reshape(len(geom2), len(freq))
        freq2 = np.tile(freq, len(geom2)).reshape(S2.shape)
        geom2 = np.repeat(geom2, len(freq2[0])).reshape(S2.shape)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        geom = [g for i, g in zip([None], geom) if i == None][0]
        freq = np.repeat(freq[:, np.newaxis], geom.shape[0], axis=1).T
        geom = np.repeat(geom[:, np.newaxis], freq.shape[1], axis=1)
        S = S.reshape((geom.shape[0], geom.shape[1]))
        cs = [np.array([0, 250//len(geom2)*i/255, 250/255]) for i in range(len(geom2))]
        for s, f, g, c in zip(S2[::-1], freq2[::-1], geom2[::-1], cs[::-1]):
            ax.plot(f, g, s, color=c, linewidth=2)

        ax.set_title(self.data.format_input(rc, 'Mag'))
        ax.set_ylabel('Geom. Param.')
        ax.set_xlabel('Frequency [GHz]')
        ax.set_zlabel(self.units['Mag'])
        ax.view_init(elev=55, azim=-140)
        plt.show()

    def contour(self, rc: (int, int), type='Mag', geoms=None):
        S, freq, geom = self.data._S(rc, type, [None])
        S = np.asarray([max(s, -69) for s in S])

        xg, yg = np.meshgrid(freq, geom[0])
        S = S.reshape(xg.shape)
        zr = S[:-1, :-1]
        z_min, z_max = zr.min(), zr.max()

        fig, ax = plt.subplots()

        c = ax.pcolormesh(xg, yg, zr, cmap='RdBu', vmin=z_min, vmax=z_max, rasterized=True)
        for g, geom_val in enumerate(geoms):
            if g == 0:
                ax.axhline(round(geom_val, 2)+0.02, color='#FBFCFC', linestyle=(0, (2, 1)),
                            linewidth=1, label=f'All slices of dielectric dataset')
            elif g == 19:
                ax.axhline(round(geom_val, 2)-0.04 + 0.02, color='#FBFCFC', linestyle=(0, (2, 1)), linewidth=1)
            elif g == 9:
                ax.axhline(round(geom_val, 2), color='orange', linestyle=(0, (2, 1)),
                            linewidth=1, label=f'Geometric value: {round(geom_val, 2)}')
            else:
                ax.axhline(round(geom_val, 2), color='#FBFCFC', linestyle=(0, (2, 1)), linewidth=1)

        # set the limits of the plot to the limits of the data
        ax.axis([xg.min(), xg.max(), yg.min(), yg.max()])
        ax.set_xlabel('Frequency [GHz]')
        ax.set_ylabel('Geometric parameter $l_2$')
        ax.legend(loc='upper right')
        fig.colorbar(c, ax=ax)
        plt.tight_layout()
        plt.savefig(f'../2d/results/S(1,1)/figures/contour_MIDAS_fine_grid_dpi1000.pdf', dpi=1000)
        # plt.savefig(f'../2d/results/S(1,1)/figures/contour_dielectric.pdf')

        plt.show()

    def dataplotter_3d(self):
        # SPECIFIKT TIL 3 GEOM
        X, y, geom = D.S((1, 1), geom_index=None)
        X, y, geom = X[:112500, 1:], y[:112500], geom[1:]
        freq = np.unique(X[:, 2])
        index = range(0, 15, 3)

        f, axarray = plt.subplots(1, 5, figsize=(12, 4), sharex="col", sharey="row")

        for i, idx in enumerate(index):
            mask = X[:, 1] == geom[1][idx]
            S = y[mask]

            xg, yg = np.meshgrid(freq, geom[0])
            S = S.reshape(xg.shape)
            zr = S[:-1, :-1]
            z_min, z_max = zr.min(), zr.max()

            c = axarray[i].pcolormesh(xg, yg, zr, cmap='RdBu', vmin=-65, vmax=0, rasterized=True)

            axarray[i].set_title(r'$l_1$: ' + f'{geom[1][idx]:.2f}')
            # set the limits of the plot to the limits of the data
            axarray[i].axis([xg.min(), xg.max(), yg.min(), yg.max()])
        f.supxlabel("Frequency [GHz]", fontsize=12, x=0.51, y=0.05)
        f.supylabel(r"Geometric Parameter $l_2$", fontsize=12)
        plt.tight_layout()

        f.subplots_adjust(right=0.94)
        cbar_ax = f.add_axes([0.9535, 0.183, 0.012, 0.708])
        f.colorbar(c, cax=cbar_ax)

        plt.savefig(f'../3d/results/S(1,1)/figures/data_l2.pdf')
        plt.show()

    def dataplotter_3d_v2(self):
        # SPECIFIKT TIL 3 GEOM
        X, y, geom = D.S((1, 1), geom_index=None)
        X, y, geom = X[:112500, 1:], y[:112500], geom[1:]
        freq = np.unique(X[:, 2])
        index = range(15)

        f, axarray = plt.subplots(3, 5, figsize=(12, 10), sharex="col", sharey="row")

        for i, idx in enumerate(index):
            mask = X[:, 0] == geom[0][idx]
            S = y[mask]

            xg, yg = np.meshgrid(freq, geom[1])
            S = S.reshape(xg.shape)
            zr = S[:-1, :-1]
            z_min, z_max = zr.min(), zr.max()

            # fig, ax = plt.subplots()

            c = axarray[i//5, i % 5].pcolormesh(xg, yg, zr, cmap='RdBu', vmin=-65, vmax=0, rasterized=True)

            axarray[i//5, i % 5].set_title(r'$l_2$: ' + f'{geom[0][idx]:.2f}')
            # set the limits of the plot to the limits of the data
            axarray[i//5, i % 5].axis([xg.min(), xg.max(), yg.min(), yg.max()])
            # ax.set_xlabel('Frequency [GHz]')
            # ax.set_ylabel('Geometric Parameter ')
            # ax.legend(loc='upper right')
        # plt.title(model.name.upper())
        # plt.contour(xg, yg, S, 20, colors='green', linewidths=0.3)
        # plt.savefig(f'../2d/results/S(1,1)/figures/contour_MIDAS_fine_grid_dpi1000.pdf', dpi=1000)
        f.supxlabel("Frequency [GHz]", fontsize=12, x=0.508, y=0.02)
        f.supylabel(r"Geometric Parameter $l_1$", fontsize=12)
        plt.tight_layout()

        f.subplots_adjust(right=0.935)
        cbar_ax = f.add_axes([0.9525, 0.075, 0.015, 0.882])
        f.colorbar(c, cax=cbar_ax)

        # plt.savefig(f'../3d/results/S(1,1)/figures/data_l1_all.pdf')
        plt.show()

    def plot_heat_map(self, rc: (int, int), type='Mag', geom_indices: [int] = [None]):
        S, freq, geom = self.data._S(rc, type, geom_indices)
        geom = [g for i, g in zip(geom_indices, geom) if i == None][0]
        freq = np.repeat(freq[:, np.newaxis], geom.shape[0], axis=1).T
        geom = np.repeat(geom[:, np.newaxis], freq.shape[1], axis=1)
        S = S.reshape((geom.shape[0], geom.shape[1]))

        plt.figure()
        plt.pcolormesh(freq, geom, S)
        plt.colorbar()
        plt.title(f'True graph of S{rc}')
        plt.show()

    def plot_cube(self):
        X, y, geom = D.S((1, 1), geom_index=None)
        X, y, geom = X[:112500, 1:], y[:112500], geom[1:]

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        filt = y < -5
        y = y[filt]
        X = X[filt]

        ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=y, cmap='RdBu', s=2)
        plt.show()

if __name__ == '__main__':
    fe = fm.FontEntry(
        fname='C:\\Users\\mølle\\AppData\\Local\\Microsoft\\Windows\\Fonts\\CharterBT-Roman.otf',
        name='Bitstream Charter')
    fm.fontManager.ttflist.insert(0, fe)
    plt.rcParams['font.family'] = 'Bitstream Charter'

    SMALL_SIZE = 8
    MEDIUM_SIZE = 12
    BIGGER_SIZE = 14
    plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
    # plt.rc('legend', titlesize=MEDIUM_SIZE)    # legend fontsize
    plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

    filepath = '../Data/initial_data/'
    filename = 'MIDAS_dielectric_output_1geom_fine.csv'
    # filename = 'dielectric.csv'
    filename2 = 'dielectric.csv'
    D = Data()
    D.load(os.path.join(filepath, filename))
    (Sr, Sc), (Fn, Gn) = D.dims()

    data_plotter = Plotter(D)
    # for i in range(15):
    #     data_plotter.plot2d((1, 1), geom_indices=[i])

    D2 = Data()
    D2.load(os.path.join(filepath, filename2))
    _, _, geom = D2.S((1,1))

    data_plotter.contour((1, 1), geoms=geom)
    # data_plotter.dataplotter_3d()
    # data_plotter.plot3d((1,1))
    # data_plotter.plot3dlines((1, 1))
    # data_plotter.plot2d((1, 1), geom_indices=[0])
    # data_plotter.plot_cube()

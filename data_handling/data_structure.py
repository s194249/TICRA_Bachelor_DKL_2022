import numpy as np
import pandas as pd


class Data:

    def __init__(self):
        self.data = None

    def load(self, filename):
        if filename[-3:] == 'csv':
            self.data = pd.read_csv(filename, sep=';')
        else:
            raise Exception("Wrong file type, not a csv file.")

    def format_input(self, rc, type='Mag'):
        # format from (a,b) to "S_a,b(type)"
        return f'S_{rc[0]},{rc[1]}({type})'

    def format_header(self, header):
        return int(header[2]), int(header[4])

    def dims(self):
        # Check if any data is loaded
        if self.data is None:
            return None
        # Extract dimensions of S matrix
        S_rc = self.format_header(self.data.columns[-1])

        # Extract dimensions of data (samples of frequency, samples of geometric parameter)
        D_rc = (self.data['Freq'].nunique(), self.data.iloc[:, 0].nunique())
        return S_rc, D_rc

    def S(self, rc: (int, int), type: str = 'Mag', geom_index: int = None):
        # if no data has been loaded yet
        if self.data is None:
            return None
        # get name of column in df
        col_name = self.format_input(rc, type.capitalize())

        freq_idx = self.data.columns.get_loc('Freq')

        # if data from all values of the geometric parameter is wanted
        if geom_index is None:
            S = np.asarray(self.data[col_name])
            parameters = np.asarray(self.data.iloc[:, :freq_idx+1])

            if self.data.columns[1] == 'Freq':
                geom_val = self.data[self.data.columns[0]].unique()
            else:
                geom_val = []
                for col in self.data.columns:
                    if col == 'Freq':
                        break
                    geom_val.append(self.data[col].unique())
            # geom_val = self.data[self.data.columns[0]].unique()
            return parameters, S, geom_val

        # if only data from a certain value of the geometric parameter is wanted
        geom = self.data[self.data.columns[0]].unique()[geom_index]
        freq = np.asarray(self.data.loc[self.data.iloc[:, 0] == geom, 'Freq'])
        S = np.asarray(self.data.loc[self.data.iloc[:, 0] == geom, col_name])
        return freq, S, geom

    def _S(self, rc: (int, int), type: str = 'Mag', geom_index: [int] = [None]):
        # if no data has been loaded yet
        if self.data is None:
            return None
        # get name of column in df
        col_name = self.format_input(rc, type.capitalize())
        freq = self.data['Freq'].unique()
        freq_idx = self.data.columns.get_loc('Freq')

        # See if enough geom indices are chosen
        if len(geom_index) != freq_idx:
            raise Exception('Not enough geom indices chosen')

        # Mask the data so only the right values of the geometric parameters are chosen
        mask = np.full(len(self.data), True)
        geom = []
        for i, g in enumerate(geom_index):
            # If all values of a geometric parameter is wanted
            if g is None:
                geom.append(self.data[self.data.columns[0]].unique())
                continue
            g = self.data[self.data.columns[i]].unique()[g]
            geom.append(g)
            mask = np.logical_and(mask, self.data.iloc[:, i] == g)

        # Get values of S based on the mask
        S = np.asarray(self.data.loc[mask, col_name])
        return S, freq, geom

This is the git repository appertaining the bachelor thesis "Modelling high-frequency data with deep kernel learning", 

made by
 - Kasper Niklas Kjær Hansen
 - Kristian Rhindal Møllmann
 - Kristoffer Marboe

supervised by
- Tue Herlau (tuhe@dtu.dk), DTU 

in collaboration with external supervisor
- Lasse Hjuler Christiansen (lhch@ticra.com), TICRA
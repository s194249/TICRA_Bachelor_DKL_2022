import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import font_manager as fm
from Utils import *

matplotlib.rc('xtick', labelsize=5)
matplotlib.rc('ytick', labelsize=5)

fe = fm.FontEntry(
    fname='C:\\Users\\mølle\\AppData\\Local\\Microsoft\\Windows\\Fonts\\CharterBT-Roman.otf',
    name='Bitstream Charter')
fm.fontManager.ttflist.insert(0, fe)
plt.rcParams['font.family'] = 'Bitstream Charter'

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 16
plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
# plt.rc('legend', titlesize=MEDIUM_SIZE)    # legend fontsize
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title


df = pd.read_csv('results/S(1,1)/n10_Fn1000_DKL_SM_DKL_RBF_DKL_Matern.csv', index_col=0, header=[0, 1])
c_palette = ['#EC7063', '#3498DB', '#9B59B6', '#2ECC71', '#F1C40F']
c_palette.reverse()

models = [col[0] for col in df.columns if col[1] == 'pred']

error_df = pd.DataFrame(None, columns=['geom'] + ['init'] + models, index=range(500))
#
error_df['geom'] = np.repeat(np.arange(0, 20, 2), 50)
error_df['init'] = np.tile(np.arange(10), 50)

for model in models:
    if all(df[(model, 'pred')].isna()):
        continue
    total_errors = []
    for geom in df.index.unique():
        # plt.subplot(4, 5, geom+1)
        f, axarray = plt.subplots(5, 2, sharex="col", sharey="row", dpi=200)
        f.suptitle(f'Model predictions for {model}, 10 init, geom {geom}')

        for cv in df[('data', 'cv')].unique():
            errors = np.array([])
            start = cv * df[('X', '0')].nunique() // df[('data', 'cv')].nunique()
            end = (cv + 1) * df[('X', '0')].nunique() // df[('data', 'cv')].nunique()
            for i_init in df[('data', 'init')].unique():
                sub_df = df[(df.index == geom) & (df[('data', 'cv')] == cv) & (df[('data', 'init')] == i_init)]
                X = np.array(sub_df['X']).flatten()
                pred = sub_df[(model, 'pred')]
                y = sub_df[('truth', 'y')]
                errors = np.append(errors, evaluate_1d(pred[start:end], y[start:end], X[start:end], metric='l1'))

            sorting = errors.argsort()
            for k, i in enumerate(sorting[:5]):
                sub_df = df[(df.index == geom) & (df[('data', 'cv')] == cv) & (df[('data', 'init')] == i)]
                X = sub_df['X']
                pred = sub_df[(model, 'pred')]
                y = sub_df[('truth', 'y')]
                axarray[cv // 2, cv % 2].plot(X, pred, linewidth=0.8)
                total_errors.append(errors[i])

            axarray[cv//2, cv % 2].plot(X[:start], y[:start], '--k', linewidth=0.9, label='training points', alpha=0.5)
            axarray[cv // 2, cv % 2].plot(X[start:end], y[start:end], '--r', linewidth=0.9, label='test points', alpha=0.5)
            axarray[cv // 2, cv % 2].plot(X[end:], y[end:], '--k', linewidth=0.9, alpha=0.5)

        f.tight_layout()
        #plt.savefig(f"results/S(1,1)/figures/n10_Fn1000_{model}.pdf")
        plt.show()
    error_df[model] = total_errors
    print(f'Model: {model}\n\t Mean error: {np.mean(total_errors):.4f}')
#error_df.groupby(["geom", "init"]).mean().to_csv(f"results/S(1,1)/error_df_Fn{1000}_{'_'.join(models)}.csv", index=True)
print(f'Mean:\n{error_df.groupby(["geom", "init"]).mean().mean()}')
print(f'STD:\n{error_df.groupby(["geom", "init"]).mean().std()}')

import os

import matplotlib.pyplot as plt
import numpy as np
import matplotlib
from matplotlib.patches import Rectangle, CirclePolygon
from matplotlib.legend_handler import HandlerBase

from data_handling.data_structure import Data
from Models.DKL.DKLModel import DKLModel
from Models.DKL.DKLKernels import *
from Utils import *
from matplotlib import font_manager as fm
if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')



class HandlerColormap(HandlerBase):
    def __init__(self, cmap, num_stripes=4, **kw):
        HandlerBase.__init__(self, **kw)
        self.cmap = cmap
        self.n = len(cmap)
        self.num_stripes = num_stripes
    def create_artists(self, legend, orig_handle,
                       xdescent, ydescent, width, height, fontsize, trans):
        stripes = []
        for i in range(self.num_stripes):
            s = Rectangle([xdescent + i * width / self.num_stripes, ydescent],
                          width / self.num_stripes,
                          height,
                          fc=self.cmap[i * self.n // (i+1)],
                          transform=trans)
            stripes.append(s)
        return stripes

class HandlerColormap2(HandlerBase):
    def __init__(self, cmap, num_stripes=4, **kw):
        HandlerBase.__init__(self, **kw)
        self.cmap = cmap
        self.n = len(cmap)
        self.num_stripes = num_stripes
    def create_artists(self, legend, orig_handle,
                       xdescent, ydescent, width, height, fontsize, trans):
        stripes = []
        for i in range(self.num_stripes):
            s = CirclePolygon([xdescent + i * width / self.num_stripes + 2, ydescent + 3],
                          width / self.num_stripes,
                          fc=self.cmap[i * self.n // (i+1)],
                          transform=trans)
            stripes.append(s)
        return stripes


matplotlib.rc('xtick', labelsize=5)
matplotlib.rc('ytick', labelsize=5)

fe = fm.FontEntry(
    fname='C:\\Users\\krism\\AppData\\Local\\Microsoft\\Windows\\Fonts\\CharterBT-Roman.otf',
    name='Bitstream Charter')
fm.fontManager.ttflist.insert(0, fe)
plt.rcParams['font.family'] = 'Bitstream Charter'

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 16
plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
# plt.rc('legend', titlesize=MEDIUM_SIZE)    # legend fontsize
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

# Load data
filepath = '../Data/'
filename = 'dielectric.csv'

geom = 8
cv = 4
n = 3

D = Data()
D.load(os.path.join(filepath, filename))
(Sr, Sc), (Fn, Gn) = D.dims()
X, y, geoms = D.S((1, 1), geom_index=geom)
X, y = X[:-1], y[:-1]
Fn -= 1
data_is_coarse = False

if data_is_coarse:
    every_nth = 10
    X, y = X[::every_nth], y[::every_nth]
    Fn = Fn // every_nth

CVs = 10


# DKL
dkl_training_iterations = 100

# DKP SM
def create_dkl_SM_model():
    DKL_SM_noise = 0.5
    DKL_SM_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_SM_likelihood.noise = DKL_SM_noise
    DKL_SM_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_SM_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=SpectralMixtureGPModel,
                    training_iterations=dkl_training_iterations)

# DKL RBF
def create_dkl_RBF_model():
    DKL_RBF_noise = 0.5
    DKL_RBF_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_RBF_likelihood.noise = DKL_RBF_noise
    DKL_RBF_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_RBF_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=RBFGPModel,
                    training_iterations=dkl_training_iterations)

# DKL Matern
def create_dkl_Matern_model():
    DKL_MATERN_noise = 0.5
    DKL_MATERN_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_MATERN_likelihood.noise = DKL_MATERN_noise
    DKL_MATERN_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_MATERN_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=MaternGPModel,
                    training_iterations=dkl_training_iterations)

models = [create_dkl_Matern_model()]

model_names = ['DKL (Matern)']

for model in models:
    filter = np.array([False if i < cv * Fn // CVs or i > (cv + 1) * Fn // CVs else True for i in range(Fn)])
    X_train, y_train = X[~filter], y[~filter]
    X_test, y_test = X[filter], y[filter]

    ming, maxg = min(X_test), max(X_test)
    ming1, maxg1 = min(X_train), max(X_train)

    zf = np.asarray(
        [[250 / 255, (gf - ming) / (maxg - ming) * 250 / 255, 0] for gf
         in X_test])
    zf1 = np.asarray(
        [[0, (gf - ming1) / (maxg1 - ming1) * 250 / 255, 250 / 255] for gf
         in X_train])

    torch.manual_seed(42 + n * 42 + 123 * geom)
    model.load_model(X_train, y_train, f'results/S(1,1)/models/{model.name}/g{geom}_cv{cv}_n{n}.pth')
    #model.fit(X_train, y_train, grid=200)

    pred, std = model.predict(X, y, return_std=True)

    plt.plot(X, pred)
    plt.plot(X, y, c='k')
    plt.fill_between(X, pred - std, pred + std, color='lightblue', alpha=0.5)
    plt.show()


    sxy1, sy1 = model.standardise(X_train, y_train)
    xy1 = model.model.scale_to_bounds(
         model.model.feature_extractor(torch.tensor(sxy1).float().to(device).reshape(-1, 1))).cpu().detach().numpy()
    #xy1 = model.model.feature_extractor(torch.tensor(sxy1).float().to(device).reshape(-1, 1)).cpu().detach().numpy()

    sxy2, sy2 = model.standardise(X_test, y_test)
    xy2 = model.model.scale_to_bounds(
        model.model.feature_extractor(torch.tensor(sxy2).float().to(device).reshape(-1, 1))).cpu().detach().numpy()
    # xy2 = model.model.feature_extractor(torch.tensor(sxy2).float().to(device).reshape(-1, 1)).cpu().detach().numpy()
    fig, axes = plt.subplots(1, 2, figsize=(8, 2.4), sharey=True, dpi=200)



    axes[0].scatter(X_train, y_train, c=zf1, s=2, label='Training')
    axes[0].scatter(X_test, y_test, c=zf, s=2, label='Test')
    axes[0].set_ylabel('Magnitude [dB]')
    axes[0].set_xlabel('Frequency [GHz]')
    cmap_labels = ['Training', 'Test']
    # create proxy artists as handles:
    # cmap_handles = [Rectangle((0, 0), 1, 1) for _ in cmap_labels]
    cmap_handles = [Rectangle((0, 0), 1, 1) for _ in cmap_labels]
    handler_map = dict(zip(cmap_handles,
                           [HandlerColormap2(cm, num_stripes=9) for cm in [zf1, zf]]))
    axes[0].legend(handles=cmap_handles,
               labels=cmap_labels,
               handler_map=handler_map)

    axes[1].scatter(xy1, y_train, c=zf1, s=2, label='Training')
    axes[1].scatter(xy2, y_test, c=zf, s=2, label='Test')
    axes[1].set_xlabel('Mapped feature space')
    axes[1].legend(handles=cmap_handles,
               labels=cmap_labels,
               handler_map=handler_map)
    plt.tight_layout()

    fig.subplots_adjust(top=0.86)
    fig.suptitle('DKL Matérn feature extractor')
    plt.savefig('results/S(1,1)/figures/final_products/1d_feature_extractor.pdf')
    plt.show()



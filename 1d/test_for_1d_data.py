import os
import time

import pandas as pd

from data_handling.data_structure import Data
from Models.DKL.DKLModel import DKLModel
from Models.DKL.DKLKernels import *
from Models.Gaussian_Process_Regression.GPyR_Matern import GPyRModelMatern
from Models.Gaussian_Process_Regression.GPyR_Matern_SKI import GPyRModelMaternSKI
from Models.Gaussian_Process_Regression.GPyR_RBF import GPyRModelRBF
from Models.Gaussian_Process_Regression.GPyR_RBF_SKI import GPyRModelRBFSKI
from Models.Gaussian_Process_Regression.GPyR_SM_SKI import GPyRModelSMSKI
from Utils import *


if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

# Load data
filepath = '../Data/'
filename = 'dielectric.csv'
D = Data()
D.load(os.path.join(filepath, filename))
(Sr, Sc), (Fn, Gn) = D.dims()
Fn -= 1



### Define models ###
# DKL SM
dkl_training_iterations = 100
def create_dkl_SM_model():
    DKL_SM_noise = 0.5
    DKL_SM_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_SM_likelihood.noise = DKL_SM_noise
    DKL_SM_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_SM_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=SpectralMixtureGPModel,
                    training_iterations=dkl_training_iterations)

# DKL RBF
def create_dkl_RBF_model():
    DKL_RBF_noise = 0.5
    DKL_RBF_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_RBF_likelihood.noise = DKL_RBF_noise
    DKL_RBF_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_RBF_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=RBFGPModel,
                    training_iterations=dkl_training_iterations)

# DKL Matern
def create_dkl_Matern_model():
    DKL_MATERN_noise = 0.5
    DKL_MATERN_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_MATERN_likelihood.noise = DKL_MATERN_noise
    DKL_MATERN_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_MATERN_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=MaternGPModel,
                    training_iterations=dkl_training_iterations)

training_iterations = 100
# RBF
def create_rbf_model():
    RBF_noise = 0.5
    RBF_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    RBF_likelihood.noise = RBF_noise
    RBF_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelRBF(likelihood=RBF_likelihood, training_iterations=training_iterations)

# RBF SKI
def create_rbf_ski_model():
    RBFSKI_noise = 0.5
    RBFSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    RBFSKI_likelihood.noise = RBFSKI_noise
    RBFSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelRBFSKI(likelihood=RBFSKI_likelihood, training_iterations=training_iterations)

# MATERN
def create_matern_model():
    MATERN_noise = 0.5
    MATERN_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    MATERN_likelihood.noise = MATERN_noise
    MATERN_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelMatern(likelihood=MATERN_likelihood, training_iterations=training_iterations)

# MATERN SKI
def create_matern_ski_model():
    MATERNSKI_noise = 0.5
    MATERNSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    MATERNSKI_likelihood.noise = MATERNSKI_noise
    MATERNSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelMaternSKI(likelihood=MATERNSKI_likelihood, training_iterations=training_iterations)

# SM SKI
def create_sm_ski_model():
    SMSKI_noise = 0.5
    SMSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    SMSKI_likelihood.noise = SMSKI_noise
    SMSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelSMSKI(likelihood=SMSKI_likelihood, training_iterations=training_iterations)


models = [create_rbf_model, create_matern_model, create_rbf_ski_model, create_matern_ski_model, create_sm_ski_model]
model_names = ['RBF', 'Matern', 'RBF_SKI', 'Matern_SKI', 'SM_SKI']
# models = [create_dkl_RBF_model, create_dkl_Matern_model, create_dkl_SM_model]
# model_names = ['DKL_RBF', 'DKL_Matern', 'DKL_SM']

# Decide number of random initialisations and CV-folds
n_inits = 1  # 10
CVs = 10

# Initialise dataframe
header = [('data', 'cv'), ('data', 'init')] + [(model_name, v) for model_name in model_names for v in ('pred', 'std')]
header += [('X', '0')] + [('truth', 'y')]
header = pd.MultiIndex.from_tuples(header)
index_col = np.repeat(range(Gn), Fn * n_inits * CVs)
df = pd.DataFrame(None, columns=header, index=index_col)

df[('data', 'cv')] = np.tile(np.repeat(range(CVs), n_inits * Fn), Gn)
df[('data', 'init')] = np.tile(np.repeat(range(n_inits), Fn), Gn * CVs)


for model_gen, model_name in zip(models, model_names):

    print('='*50+'\nTraining', model_name, 'model')

    for geom_index in range(0, Gn, 2):
        print(f'\tGeometric index: {geom_index+1}/{Gn}')
        # load in data
        X, y, geom = D.S((1, 1), geom_index=geom_index)
        X = X[:-1]
        y = y[:-1]
        data_is_coarse = False
        if data_is_coarse:
            every_nth = 20
            X, y = X[::every_nth], y[::every_nth]
            Fn = Fn // every_nth

        bounds = np.array([X.min(), X.max()])

        for cv in range(CVs):
            print(f'\t\tCV-fold: {cv + 1}/{CVs}')
            # Pick out one segment of the signal
            filter = np.array([False if i < cv * Fn//CVs or i > (cv + 1) * Fn//CVs else True for i in range(Fn)])
            X_train, y_train = X[~filter], y[~filter]
            X_test, y_test = X[filter], y[filter]

            for i in range(n_inits):
                time.sleep(0.01)
                print(f'\t\t\tRandom run: {i+1}/{n_inits}')
                torch.manual_seed(42 + i*42 + 123*geom_index)
                model = model_gen()
                # Fit model and do predictions
                loss = model.fit(X_train, y_train, grid=200, bounds=bounds)
                pred, std = model.predict(X, y, return_std=True)

                start = (geom_index * CVs * n_inits + cv * n_inits + i) * Fn
                end = start + Fn
                df[(model_name, 'pred')].values[start: end] = pred
                df[(model_name, 'std')].values[start: end] = std
                df[('X', '0')].values[start: end] = X
                df[('truth', 'y')].values[start: end] = y

                model.save_model(f'results/S(1,1)/models/{model.name}/g{geom_index}_cv{cv}_n{i}')

            df.to_csv(f"results/S(1,1)/n{n_inits}_Fn{Fn}_{'_'.join(model_names)}.csv", index=True)

    print('')

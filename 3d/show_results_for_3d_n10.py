import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from Utils import *


df = pd.read_csv('results/S(1,1)/n10_Fn100_DKL_RBF_DKL_Matern_DKL_SM.csv', index_col=0, header=[0, 1])

models = [col[0] for col in df.columns if col[1] == 'pred']

error_df = pd.DataFrame(None, columns=['geom'] + ['init'] + models, index=range(50))

error_df['geom'] = np.append(np.repeat(np.arange(0, 15, 3), 5), np.repeat(np.arange(0, 15, 3) + 0.5 + 15, 5))
error_df['init'] = np.tile(np.arange(5), 10)

for model in models:
    # print(model)
    model_errors = []
    for k, geom_param in enumerate([('data', 'l1'), ('data', 'l2')]):
        # print(f'\tk: {k}')
        for i, geom in enumerate(df[geom_param].dropna().unique()):
            # print(f'\t\tgeom: {geom}')
            geom_df = df[df[geom_param] == geom]
            errors = np.array([])
            for i_init in geom_df[('data', 'init')].unique():
                sub_df = geom_df[geom_df[('data', 'init')] == i_init]
                X_test = np.array(sub_df[[('X', f'{k}'), ('X', '2')]])
                preds = np.array(sub_df[(model, 'pred')])
                y_test = np.array(sub_df[('truth', 'y')])
                dx = []
                dx.append(np.unique(X_test[:, 0])[1] - np.unique(X_test[:, 0])[0])
                dx.append(np.unique(X_test[:, 1])[1] - np.unique(X_test[:, 1])[0])
                errors = np.append(errors, evaluate_2d(preds, y_test, dx=dx, num_grid=(15, 100)))
                # print(f'\t\t\ti_init, error: {i_init, evaluate_2d(preds, y_test, dx=dx, num_grid=(15, 100))}')

            sorting = errors.argsort()

            for _, j in enumerate(sorting[:5]):
                model_errors.append(errors[j])
    error_df[model] = model_errors

error_df.groupby(["geom"]).mean().to_csv(f"results/S(1,1)/error_df_Fn{100}_{'_'.join(models)}.csv", index=True)
print(f'Mean:\n{error_df.groupby(["geom"]).mean().iloc[:, 1:].mean()}')
print(f'STD:\n{error_df.groupby(["geom"]).mean().iloc[:, 1:].std()}')

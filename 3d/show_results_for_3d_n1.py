import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from Utils import *


df = pd.read_csv('results/S(1,1)/n1_Fn100_RBF_RBF_SKI_Matern_Matern_SKI_SM_SKI_corrected.csv', index_col=0, header=[0, 1])

models = [col[0] for col in df.columns if col[1] == 'pred']

error_df = pd.DataFrame(None, columns=models, index=range(10))

for model in models:
    model_errors = []
    for k, geom_param in enumerate([('data', 'l1'), ('data', 'l2')]):
        for i, geom in enumerate(df[geom_param].dropna().unique()):
            geom_df = df[df[geom_param] == geom]

            preds = np.array(geom_df[(model, 'pred')])
            y_test = np.array(geom_df[('truth', 'y')])
            X_test = np.array(geom_df[[('X', f'{k}'), ('X', '2')]])

            dx = []
            dx.append(np.unique(X_test[:, 0])[1]  - np.unique(X_test[:, 0])[0])
            dx.append(np.unique(X_test[:, 1])[1] - np.unique(X_test[:, 1])[0])
            model_errors.append(evaluate_2d(preds, y_test, dx=dx, num_grid=(15, 100)))
    error_df[model] = model_errors

# error_df.to_csv(f"results/S(1,1)/error_df_Fn{100}_{'_'.join(models)}.csv", index=True)
print(f'Mean:\n{error_df.mean()}')
print(f'STD:\n{error_df.std()}')

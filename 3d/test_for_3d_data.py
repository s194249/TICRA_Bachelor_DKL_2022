import os
import time

import numpy as np

from data_handling.data_structure import Data
from Models.DKL.DKLModel import DKLModel
from Models.DKL.DKLKernels import *
from Models.Gaussian_Process_Regression.GPyR_Matern import GPyRModelMatern
from Models.Gaussian_Process_Regression.GPyR_Matern_SKI import GPyRModelMaternSKI
from Models.Gaussian_Process_Regression.GPyR_RBF import GPyRModelRBF
from Models.Gaussian_Process_Regression.GPyR_RBF_SKI import GPyRModelRBFSKI
from Models.Gaussian_Process_Regression.GPyR_SM_SKI import GPyRModelSMSKI
from Utils import *
from itertools import product


if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

# Load data
filepath = '../Data/'
filename = 'MIDAS_dielectric_output_3geom.csv'
D = Data()
D.load(os.path.join(filepath, filename))
(Sr, Sc), (Fn, Gn) = D.dims()
X, y, geom = D.S((1, 1), geom_index=None)
X, y, geom = X[:112500, 1:], y[:112500], geom[1:]
data_is_coarse = True
Gn_new = Gn

if data_is_coarse:
    every_nth = 5
    X, y = X[::every_nth], y[::every_nth]
    Fn = Fn // every_nth
    Gn_new = 5

### Define models ###
# DKL SM
dkl_training_iterations = 100
def create_dkl_SM_model():
    DKL_SM_noise = 0.5
    DKL_SM_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_SM_likelihood.noise = DKL_SM_noise
    DKL_SM_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_SM_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=SpectralMixtureGPModel,
                    training_iterations=dkl_training_iterations)

# DKL RBF
def create_dkl_RBF_model():
    DKL_RBF_noise = 0.5
    DKL_RBF_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_RBF_likelihood.noise = DKL_RBF_noise
    DKL_RBF_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_RBF_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=RBFGPModel,
                    training_iterations=dkl_training_iterations)

# DKL Matern
def create_dkl_Matern_model():
    DKL_MATERN_noise = 0.5
    DKL_MATERN_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_MATERN_likelihood.noise = DKL_MATERN_noise
    DKL_MATERN_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_MATERN_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=MaternGPModel,
                    training_iterations=dkl_training_iterations)

# DKL Matern NO SKI
def create_dkl_Matern_no_ski_model():
    DKL_MATERN_NO_SKI_noise = 0.5
    DKL_MATERN_NO_SKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_MATERN_NO_SKI_likelihood.noise = DKL_MATERN_NO_SKI_noise
    DKL_MATERN_NO_SKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_MATERN_NO_SKI_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=MaternGPModelNOSKI,
                    training_iterations=dkl_training_iterations)

n_training_iters = 100
# RBF
def create_rbf_model():
    RBF_noise = 0.5
    RBF_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    RBF_likelihood.noise = RBF_noise
    RBF_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelRBF(likelihood=RBF_likelihood, training_iterations=n_training_iters)

# RBF SKI
def create_rbf_ski_model():
    RBFSKI_noise = 0.5
    RBFSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    RBFSKI_likelihood.noise = RBFSKI_noise
    RBFSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelRBFSKI(likelihood=RBFSKI_likelihood, training_iterations=n_training_iters)

# MATERN
def create_matern_model():
    MATERN_noise = 0.5
    MATERN_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    MATERN_likelihood.noise = MATERN_noise
    MATERN_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelMatern(likelihood=MATERN_likelihood, training_iterations=n_training_iters)

# MATERN SKI
def create_matern_ski_model():
    MATERNSKI_noise = 0.5
    MATERNSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    MATERNSKI_likelihood.noise = MATERNSKI_noise
    MATERNSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelMaternSKI(likelihood=MATERNSKI_likelihood, training_iterations=n_training_iters)

# SM SKI
def create_sm_ski_model():
    SMSKI_noise = 0.5
    SMSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    SMSKI_likelihood.noise = SMSKI_noise
    SMSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelSMSKI(likelihood=SMSKI_likelihood, training_iterations=n_training_iters)

# models = [create_dkl_RBF_model, create_dkl_Matern_model, create_dkl_SM_model]
# model_names = ['DKL_RBF', 'DKL_Matern', 'DKL_SM']
models = [create_rbf_model, create_rbf_ski_model, create_matern_model, create_matern_ski_model, create_sm_ski_model]
model_names = ['RBF', 'RBF_SKI', 'Matern', 'Matern_SKI', 'SM_SKI']

# Decide number of random initialisations,
n_inits = 1 # 10
grid = 50

# Initialise dataframe
header = [('data', 'l2'), ('data', 'l1'), ('data', 'init')] + [(model_name, v) for model_name in model_names for v in ('pred', 'std')]
header += [('X', str(i)) for i in range(X.shape[-1])] + [('truth', 'y')]
header = pd.MultiIndex.from_tuples(header)
df = pd.DataFrame(None, columns=header)

geoms = np.repeat(range(0, Gn, 3), n_inits * Fn * Gn)
nans = np.empty(len(geoms))
nans[:] = np.NaN
df[('data', 'l2')] = np.concatenate((geoms, nans))
df[('data', 'l1')] = np.concatenate((nans, geoms))

df[('data', 'init')] = np.tile(np.repeat(range(n_inits), Fn*Gn), Gn_new*2)

bounds = np.array([[min(X[:, 0]), max(X[:, 0])], [min(X[:, 1]), max(X[:, 1])], [min(X[:, 2]), max(X[:, 2])]])

for model_gen, model_name in zip(models, model_names):
    print('='*50+'\nTraining', model_name, 'model')
    # Initialise arrays for results
    n_preds = Fn * Gn
    n_runs = Gn_new * 2 * n_inits
    all_geom_preds = np.zeros(n_preds * n_runs)
    all_geom_stds = np.zeros(n_preds * n_runs)
    all_geom_Xs = [np.zeros(n_preds * n_runs) for _ in range(X.shape[-1])]
    all_geom_y = np.zeros(n_preds * n_runs)

    for k, geom_param in enumerate(geom):
        print(f'\tGeometric parameter: {k + 1}/{len(geom)}')
        for l, geom_index in enumerate(range(0, 15, 3)):
            print(f'\t\tGeometric index: {geom_index+1}/{15}')
            # Pick out one value of the geometric parameter
            filter = X[:, k] == geom_param[geom_index]
            X_train, y_train = X[~filter], y[~filter]
            X_test, y_test = X[filter], y[filter]

            for i in range(n_inits):
                time.sleep(0.01)
                print(f'\t\t\tRandom run: {i+1}/{n_inits}')
                torch.manual_seed(42 + i*42 + 123*l)
                model = model_gen()
                # Fit model and do predictions
                model.fit(X_train, y_train, bounds=bounds, grid=grid)
                pred, std = model.predict(X_test, y_test, return_std=True)

                start_idx = (n_inits * l + i) * Fn * Gn + (Fn * Gn * Gn_new * n_inits * k)
                stop_idx = (n_inits * l + (i + 1)) * Fn * Gn + (Fn * Gn * Gn_new * n_inits * k)

                all_geom_preds[start_idx: stop_idx] = pred
                all_geom_stds[start_idx: stop_idx] = std
                for j, all_geom_X in enumerate(all_geom_Xs):
                    all_geom_X[start_idx: stop_idx] = X_test[:, j]
                all_geom_y[start_idx: stop_idx] = y_test
                model.save_model(f'results/S(1,1)/models/{model.name}/Fn{Fn}/g{geom_index}_n{i}_grid{grid}')

            df[(model_name, 'pred')] = all_geom_preds
            df[(model_name, 'std')] = all_geom_stds
            for i, all_geom_X in enumerate(all_geom_Xs):
                df[('X', str(i))] = all_geom_X
            df[('truth', 'y')] = all_geom_y

            print('')

            df.to_csv(f"results/S(1,1)/n{n_inits}_Fn{Fn}_{'_'.join(model_names)}.csv", index=True)

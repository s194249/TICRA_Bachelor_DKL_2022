import os
import time

import numpy as np

from data_handling.data_structure import Data
from Models.DKL.DKLModel import DKLModel
from Models.DKL.DKLKernels import *
from Utils import *

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

# Load data
filepath = '../Data/initial_data/'
filename = 'MIDAS_dielectric_output_3geom.csv'
D = Data()
D.load(os.path.join(filepath, filename))
(Sr, Sc), (Fn, Gn) = D.dims()
X, y, geom = D.S((1, 1), geom_index=None)
X, y, geom = X[:112500, 1:], y[:112500], geom[1:]
data_is_coarse = True
Gn_new = Gn

if data_is_coarse:
    every_nth = 5
    X, y = X[::every_nth], y[::every_nth]
    Fn = Fn // every_nth
    Gn_new = 5

### Define models ###
# DKL SM
dkl_training_iterations = 100
def create_dkl_SM_model():
    DKL_SM_noise = 0.5
    DKL_SM_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_SM_likelihood.noise = DKL_SM_noise
    DKL_SM_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_SM_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=SpectralMixtureGPModel,
                    training_iterations=dkl_training_iterations)

models = [create_dkl_SM_model]
model_names = ['DKL_SM']

# Decide number of random initialisations,
n_inducing = [10, 25, 50, 75, 100]

# Initialise dataframe
header = [('data', 'l2'), ('data', 'l1'), ('data', 'init')] + [(model_name, v) for model_name in model_names for v in ('pred', 'std')]
header += [('X', str(i)) for i in range(X.shape[-1])] + [('truth', 'y')]
header = pd.MultiIndex.from_tuples(header)
df = pd.DataFrame(None, columns=header)

geoms = np.repeat(range(0, Gn, 3), len(n_inducing) * Fn * Gn)
nans = np.empty(len(geoms))
nans[:] = np.NaN
df[('data', 'l2')] = np.concatenate((geoms, nans))
df[('data', 'l1')] = np.concatenate((nans, geoms))
df[('data', 'inducing')] = np.tile(np.repeat(n_inducing, Fn*Gn), Gn_new*2)

header2 = ['n_inducing', 'DKL_SM']
df2 = pd.DataFrame(None, columns=header2)
df2['n_inducing'] = np.tile(n_inducing, Gn_new*2)

for model_gen, model_name in zip(models, model_names):
    print('='*50+'\nTraining', model_name, 'model')
    # Initialise arrays for results
    n_preds = Fn * Gn
    n_runs = Gn_new * 2 * len(n_inducing)
    all_geom_preds = np.zeros(n_preds * n_runs)
    all_geom_stds = np.zeros(n_preds * n_runs)
    all_geom_Xs = [np.zeros(n_preds * n_runs) for _ in range(X.shape[-1])]
    all_geom_y = np.zeros(n_preds * n_runs)
    all_training_times = np.zeros(len(n_inducing) * Gn_new * 2)
    for k, geom_param in enumerate(geom):
        if k == 0:
            continue
        print(f'\tGeometric parameter: {k + 1}/{len(geom)}')
        for l, geom_index in enumerate(range(0, 15, 3)):
            print(f'\tGeometric index: {geom_index+1}/{Gn}')
            # Pick out one value of the geometric parameter
            filter = X[:, k] == geom_param[geom_index]
            X_train, y_train = X[~filter], y[~filter]
            X_test, y_test = X[filter], y[filter]

            for i, grid in enumerate(n_inducing):
                time.sleep(0.01)
                print(f'\t\tRandom run: {i+1}/{len(n_inducing)}')
                torch.manual_seed(42 + i*42 + 123*l)
                model = model_gen()
                # Fit model and do predictions
                t = time.time()
                model.fit(X_train, y_train, grid=grid)
                all_training_times[len(n_inducing)*l + i] = time.time() - t

                pred, std = model.predict(X_test, y_test, return_std=True)

                dx = []
                dx.append(np.unique(X_test[:, 1-k])[1] - np.unique(X_test[:, 1-k])[0])
                dx.append(np.unique(X_test[:, 2])[1] - np.unique(X_test[:, 2])[0])
                print(f'\t\tError: {evaluate_2d(pred, y_test, dx=dx, num_grid=(15, 100)):.3f}')

                start_idx = (len(n_inducing) * l + i) * Fn * Gn + (Fn * Gn * Gn_new * len(n_inducing) * k)
                stop_idx = (len(n_inducing) * l + (i + 1)) * Fn * Gn + (Fn * Gn * Gn_new * len(n_inducing) * k)

                all_geom_preds[start_idx: stop_idx] = pred
                all_geom_stds[start_idx: stop_idx] = std
                for j, all_geom_X in enumerate(all_geom_Xs):
                    all_geom_X[start_idx: stop_idx] = X_test[:, j]
                all_geom_y[start_idx: stop_idx] = y_test

        df[(model_name, 'pred')] = all_geom_preds
        df[(model_name, 'std')] = all_geom_stds
        for i, all_geom_X in enumerate(all_geom_Xs):
            df[('X', str(i))] = all_geom_X
        df[('truth', 'y')] = all_geom_y
        df2[model_name] = all_training_times

        print('')

        df.to_csv(f"results/S(1,1)/inducing_points/predictions_2.csv", index=True)
        df2.to_csv(f'results/S(1,1)/inducing_points/training_times_2.csv', index=True)

from __future__ import annotations
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.interpolate import interp1d, LinearNDInterpolator
from scipy.integrate import simpson


def plot_errors(data: pd.DataFrame, columns=None):
    plt.figure()

    if columns is None:
        for column in data.columns:
            plt.plot(data[column], label=column)
            plt.xlabel("Training fraction")
            plt.ylabel("L1 error")
            plt.legend()

    elif len(columns) > 1:
        for column in columns:
            plt.plot(data[column], label=column)
            plt.xlabel("Training fraction")
            plt.ylabel("L1 error")
            plt.legend()

    else:
        plt.plot(data[columns])
        plt.xlabel("Training fraction")
        plt.ylabel("L1 error")
        plt.title(columns)

    plt.show()


def points_to_func(X, y, num: int = 10000):
    if len(X.shape) == 1:
        f = interp1d(X, y)
        x_new = np.linspace(min(X), max(X), num=num, endpoint=True)
        y_new = f(x_new)
        dx = [x_new[1] - x_new[0]]

    else:
        f = LinearNDInterpolator(X, y)
        var_list = list(zip(*X))
        x_grid = []
        for i in range(len(var_list)):
            x_grid.append(np.linspace(min(var_list[i]), max(var_list[i]), num=num, endpoint=True))

        dx = [x_grid[i][1] - x_grid[i][0] for i in range(len(x_grid))]
        x_new = np.array([list(x) for x in np.array(np.meshgrid(*x_grid)).T.reshape(-1, len(x_grid))])
        y_new = f(x_new)

    return x_new, y_new, dx


def nd_trapezoidal_integration(dx: [float], y, num_grid):
    s = tuple([slice(None, -1)]*len(dx))
    s2 = tuple([slice(1, None)]*len(dx))
    y = y.reshape(num_grid)
    y_s = (y[s] + y[s2]) / 2
    return (y_s * np.prod(dx)).sum()


def evaluate_1d(y_pred, y_test, X_test=None, metric: str = 'tue'):
    """
    Evaluate the prediction accuracy of the model
    :param y_pred:  (array) Model predictions
    :param y_test: (array) True function values
    :param metric: (str) Type of error metric
    :return: (float) Prediction error
    """
    metric = metric.lower()
    if metric == 'l1':
        error = np.mean(abs(y_test - y_pred))
    elif metric == 'l2':
        error = np.mean(np.power(y_test - y_pred, 2))
    elif metric == 'tue':
        if X_test is None:
            raise ValueError('Missing X_test for integration')
        error = simpson(np.abs(y_test - y_pred), X_test) / simpson(np.abs(y_test), X_test)
    elif metric == 'wape':
        error = np.sum(np.abs(y_test - y_pred)) / np.sum(np.abs(y_test))
    else:
        raise ValueError('Error metric unknown')
    return error


def evaluate_2d(y_pred, y_test, dx: [float], num_grid, metric='tue'):
    if metric == 'tue':
        error = nd_trapezoidal_integration(dx=dx, y=np.abs(y_test - y_pred), num_grid=num_grid) / \
                nd_trapezoidal_integration(dx=dx, y=np.abs(y_test), num_grid=num_grid)
    else:
        error = np.sum(np.abs(y_test - y_pred)) / np.sum(np.abs(y_test))
    return error


if __name__ == '__main__':
    file = pd.read_csv("Results/2d/dielectric_2d_Lin_TF(0.002;0.1).csv", index_col=0, header=[0, 1])
    plot_errors(file)
    # plot_errors(file["GPR", "LIn"])

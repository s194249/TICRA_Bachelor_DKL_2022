import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from Utils import *
import matplotlib.font_manager as fm

fe = fm.FontEntry(
        fname='C:\\Users\\mølle\\AppData\\Local\\Microsoft\\Windows\\Fonts\\CharterBT-Roman.otf',
        name='Bitstream Charter')
fm.fontManager.ttflist.insert(0, fe)
plt.rcParams['font.family'] = 'Bitstream Charter'

SMALL_SIZE = 10
MEDIUM_SIZE = 14
BIGGER_SIZE = 16
plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
# plt.rc('legend', titlesize=MEDIUM_SIZE)    # legend fontsize
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

df = pd.read_csv('results/S(1,1)/inducing_points/training_times.csv', index_col=0)
df2 = pd.read_csv('results/S(1,1)/inducing_points/predictions.csv', index_col=0, header=[0, 1])

training_times = df.groupby(by='n_inducing').mean()

error_df = pd.DataFrame(None, columns=df2[('data', 'geom')].unique(), index=df2[('data', 'inducing')].unique())
errors = np.zeros(140)

for i, geom in enumerate(df2[('data', 'geom')].unique()):
    geom_df = df2[df2[('data', 'geom')] == geom]
    for j, inducing_points in enumerate(geom_df[('data', 'inducing')].unique()):
        inducing_df = geom_df[geom_df[('data', 'inducing')] == inducing_points]
        preds = inducing_df[('DKL_SM', 'pred')]
        y_test = inducing_df[('truth', 'y')]
        X_test = inducing_df[('X', '1')]

        error_df.loc[inducing_points, geom] = evaluate_1d(preds, y_test, X_test)

error_df.values.sort(axis=1)
errors_vals = error_df.iloc[:, :10].mean(axis=1)
error_stds = error_df.iloc[:, :10].std(axis=1)
fig = plt.figure()
ax = fig.add_subplot(111)
ax.errorbar(df2[('data', 'inducing')].unique()-4, errors_vals, yerr=error_stds, fmt='none', ecolor='#3498DB',
            elinewidth=1, capsize=5)
ax.plot(df2[('data', 'inducing')].unique()-4, errors_vals, '.-', c='#3498DB', label='Error', linewidth=1)
ax2 = ax.twinx()

ax2.plot(df2[('data', 'inducing')].unique()+4, training_times.DKL_SM, '.-', c='#EC7063', linewidth=1)

ax.plot(np.nan, '.-', c='#EC7063', label="Training time")

ax.grid(alpha=0.4, linestyle='dashed')
ax.set_xlabel("#Inducing Points")
ax.set_ylabel(r"WAPE")
ax2.set_ylabel(r"Training time [s]")

ax.legend(loc='upper center')

ax.set_zorder(ax2.get_zorder()+1)  # put ax in front of ax2
ax.patch.set_visible(False)  # hide the 'canvas'

plt.xticks(df2[('data', 'inducing')].unique())
plt.tight_layout()
plt.savefig(f"results/S(1,1)/inducing_points/inducing_points_error_training_time_2d.pdf")
plt.show()

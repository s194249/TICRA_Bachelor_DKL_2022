import os
import time

import pandas as pd

from data_handling.data_structure import Data
from Models.DKL.DKLModel import DKLModel
from Models.DKL.DKLKernels import *
from Utils import *


if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

# Load data
filepath = '../Data/'
filename = 'dielectric.csv'
D = Data()
D.load(os.path.join(filepath, filename))
(Sr, Sc), (Fn, Gn) = D.dims()
X, y, geom = D.S((1, 1), geom_index=None)
X = np.delete(X, np.arange(0, X.shape[0], 1001), axis=0)
y = np.delete(y, np.arange(0, y.shape[0], 1001), axis=0)
Fn -= 1

data_is_coarse = False

if data_is_coarse:
    every_nth = 20
    X, y = X[::every_nth], y[::every_nth]
    Fn = Fn // every_nth


### Define models ###
# DKL SM
dkl_training_iterations = 100
def create_dkl_SM_model():
    DKL_SM_noise = 0.5
    DKL_SM_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_SM_likelihood.noise = DKL_SM_noise
    DKL_SM_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_SM_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=SpectralMixtureGPModel,
                    training_iterations=dkl_training_iterations)

models = [create_dkl_SM_model]
model_names = ['DKL_SM']

# Decide number of random initialisations,
n_inducing = [10, 50, 100, 200, 400, 800, 1000]

# Initialise dataframe
header = [('data', 'geom'), ('data', 'init')] + [(model_name, v) for model_name in model_names for v in ('pred', 'std')]
header += [('X', str(i)) for i in range(X.shape[-1])] + [('truth', 'y')]
header = pd.MultiIndex.from_tuples(header)
df = pd.DataFrame(None, columns=header)

df[('data', 'geom')] = np.repeat(np.arange(Gn), len(n_inducing) * Fn)
df[('data', 'inducing')] = np.tile(np.repeat(n_inducing, Fn), Gn)


header2 = ['n_inducing', 'DKL_SM']
df2 = pd.DataFrame(None, columns=header2, index=np.repeat(np.arange(Gn), len(n_inducing)))
df2['n_inducing'] = np.tile(n_inducing, Gn)


for model_gen, model_name in zip(models, model_names):
    print('='*50+'\nTraining', model_name, 'model')
    # Initialise arrays for results
    all_geom_preds = np.zeros(Fn * Gn * len(n_inducing))
    all_geom_stds = np.zeros(Fn * Gn * len(n_inducing))
    all_geom_Xs = [np.zeros(Fn * Gn * len(n_inducing)) for _ in range(X.shape[-1])]
    all_geom_y = np.zeros(Fn * Gn * len(n_inducing))
    all_training_times = np.zeros(len(n_inducing) * Gn)

    for geom_index in range(Gn):
        print(f'\tGeometric index: {geom_index+1}/{Gn}')
        # Pick out one value of the geometric parameter
        filter = X[:, 0] == geom[geom_index]
        X_train, y_train = X[~filter], y[~filter]
        X_test, y_test = X[filter], y[filter]

        for i, grid in enumerate(n_inducing):
            time.sleep(0.01)
            print(f'\t\tRandom run: {i+1}/{len(n_inducing)}')
            torch.manual_seed(42 + i*42 + 123*geom_index)
            model = model_gen()
            # Fit model and do predictions
            t = time.time()
            model.fit(X_train, y_train, grid=grid)
            all_training_times[len(n_inducing)*geom_index + i] = time.time() - t

            pred, std = model.predict(X_test, y_test, return_std=True)
            print(f'\t\tError: {evaluate_1d(pred, y_test, X_test[:, 1]):.3f}')

            all_geom_preds[(len(n_inducing) * geom_index + i) * Fn: (len(n_inducing) * geom_index + (i + 1)) * Fn] = pred
            all_geom_stds[(len(n_inducing) * geom_index + i) * Fn: (len(n_inducing) * geom_index + (i + 1)) * Fn] = std
            for j, all_geom_X in enumerate(all_geom_Xs):
                all_geom_X[(len(n_inducing) * geom_index + i) * Fn: (len(n_inducing) * geom_index + (i + 1)) * Fn] = X_test[:, j]
            all_geom_y[(len(n_inducing) * geom_index + i) * Fn: (len(n_inducing) * geom_index + (i + 1)) * Fn] = y_test
            # model.save_model(f'results/S(1,1)/{model.name}/g{geom_index}_n{i}')

    df[(model_name, 'pred')] = all_geom_preds
    df[(model_name, 'std')] = all_geom_stds
    for i, all_geom_X in enumerate(all_geom_Xs):
        df[('X', str(i))] = all_geom_X
    df[('truth', 'y')] = all_geom_y
    df2[model_name] = all_training_times

    print('')

    df.to_csv(f"results/S(1,1)/inducing_points/predictions.csv", index=True)
    df2.to_csv(f'results/S(1,1)/inducing_points/training_times.csv', index=True)

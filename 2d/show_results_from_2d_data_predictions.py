import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.font_manager as fm
from Utils import *

matplotlib.rc('xtick', labelsize=5)
matplotlib.rc('ytick', labelsize=5)

fe = fm.FontEntry(
    fname='C:\\Users\\mølle\\AppData\\Local\\Microsoft\\Windows\\Fonts\\CharterBT-Roman.otf',
    name='Bitstream Charter')
fm.fontManager.ttflist.insert(0, fe)
plt.rcParams['font.family'] = 'Bitstream Charter'

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 12
plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
# plt.rc('legend', titlesize=MEDIUM_SIZE)    # legend fontsize
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

df = pd.read_csv('results/S(1,1)/n1_Fn1000_RBF_RBF_SKI_Matern_Matern_SKI_SM_SKI.csv', index_col=0, header=[0, 1])
c_palette = ['#EC7063', '#3498DB', '#9B59B6', '#2ECC71', '#F1C40F']
# c_palette.reverse()
to_mean = True

models = [col[0] for col in df.columns if col[1] == 'pred']

error_df = pd.DataFrame(None, columns=models)

f, axarray = plt.subplots(len(models), 4, sharex="col", sharey="row", figsize=(8, 6))
models = ['RBF', 'Matern', 'RBF_SKI', 'Matern_SKI', 'SM_SKI']

for l, model in enumerate(models):
    if any(df[(model, 'pred')].isna()):
        continue

    total_errors = []
    for j, geom in enumerate([0, 6, 13, 19]):
        # plt.subplot(4, 5, geom+1)
        errors = np.array([])

        for i_init in df[('data', 'init')].unique():
            sub_df = df[(df[('data', 'geom')] == geom) & (df[('data', 'init')] == i_init)]
            X = np.array(sub_df['X'])
            pred = sub_df[(model, 'pred')]
            y = sub_df[('truth', 'y')]
            errors = np.append(errors, evaluate_1d(pred, y, X[:, 1]))

        sorting = errors.argsort()

        for k, i in enumerate(sorting[:5]):
            sub_df = df[(df[('data', 'geom')] == geom) & (df[('data', 'init')] == i)]
            X = np.array(sub_df['X'])
            pred = sub_df[(model, 'pred')]
            std = sub_df[(model, 'std')]
            y = sub_df[('truth', 'y')]
            axarray[l, j].plot(X[:, 1], pred, c=c_palette[k], linewidth=0.8, alpha=0.6)
            total_errors.append(errors[i])
        if l == 0:
            axarray[l, j].set_title(f"$l_2$: {sub_df[('X', '0')].values[0]:.2f}", fontsize=10)
        if j == 3:
            axarray[l, j].yaxis.set_label_position("right")
            axarray[l, j].set_ylabel(f"{model.replace('_', ' ')}", fontsize=8, labelpad=6)  # 6

        std = (std / np.std(std)) * 3
        axarray[l, j].plot(X[:, 1], y, c='#3D3E3F', linestyle=(0, (3, 1)), linewidth=0.2, label='true function', alpha=0.5)

    error_df[model] = total_errors

f.supxlabel("Frequency [GHz]", fontsize=10, x=0.53, y=0.04)
f.supylabel("Magnitude [dB]", fontsize=10, x=0.03, y=0.53)
f.tight_layout()
plt.savefig(f"results/S(1,1)/figures/final_products/Fn1000_{'_'.join(models)}.pdf", bbox_inches='tight')

plt.show()
print(f'Mean:\n{error_df.mean()}')
print(f'STD:\n{error_df.std()}')

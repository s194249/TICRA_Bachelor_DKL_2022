import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.font_manager as fm
from Utils import *

matplotlib.rc('xtick', labelsize=5)
matplotlib.rc('ytick', labelsize=5)

fe = fm.FontEntry(
    fname='C:\\Users\\mølle\\AppData\\Local\\Microsoft\\Windows\\Fonts\\CharterBT-Roman.otf',
    name='Bitstream Charter')
fm.fontManager.ttflist.insert(0, fe)
plt.rcParams['font.family'] = 'Bitstream Charter'

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 12
plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
# plt.rc('legend', titlesize=MEDIUM_SIZE)    # legend fontsize
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

df = pd.read_csv('results/S(1,1)/n1_Fn1000_RBF_RBF_SKI_Matern_Matern_SKI_SM_SKI.csv', index_col=0, header=[0, 1])
geom = 6
c_palette = ['#EC7063', '#3498DB', '#9B59B6', '#2ECC71', '#F1C40F']
# c_palette.reverse()
to_mean = False

models = [col[0] for col in df.columns if col[1] == 'pred']

f, axarray = plt.subplots(2, 4, sharex="col", sharey="row", figsize=(8, 4))
models = ['RBF', 'Matern', 'RBF_SKI', 'Matern_SKI', 'SM_SKI']

row = [0, 0, 1, 1, 1]
col = [0, 1, 0, 1, 2]


for l, model in enumerate(models):
    if any(df[(model, 'pred')].isna()):
        continue

    sub_df = df[(df[('data', 'geom')] == geom)]
    X = np.array(sub_df['X'])
    pred = sub_df[(model, 'pred')]
    std = sub_df[(model, 'std')]
    y = sub_df[('truth', 'y')]

    axarray[row[l], col[l]].plot(X[:, 1], pred, c=c_palette[0], linewidth=0.8, alpha=0.6)
    axarray[row[l], col[l]].fill_between(X[:, 1], pred - 2 * std, pred + 2 * std,
                                         color='lightblue', alpha=0.5,
                                         label=r"$2\sigma$")

    std = (std / np.std(std)) * 3
    if models[l] == 'RBF_SKI':
        axarray[row[l], col[l]].plot(X[:, 1], std - (np.max(std) + 60), linewidth=0.1, alpha=0.6, c='k')
    elif models[l] == 'Matern_SKI':
        axarray[row[l], col[l]].plot(X[:, 1], std - (np.max(std) + 50), linewidth=0.1, alpha=0.6, c='k')
    elif models[l] == 'Matern':
        axarray[row[l], col[l]].plot(X[:, 1], std - (np.max(std) + 40), linewidth=0.1, alpha=0.6, c='k')
    else:
        axarray[row[l], col[l]].plot(X[:, 1], std - (np.max(std) + 50), linewidth=0.1, alpha=0.6, c='k')
    axarray[row[l], col[l]].plot(X[:, 1], y, c='#3D3E3F', linestyle=(0, (3, 1)), linewidth=0.2, label='true function', alpha=0.5)
    axarray[row[l], col[l]].set_title(models[l].replace('_', ' '))

df = pd.read_csv('results/S(1,1)/n10_Fn1000_DKL_SM_DKL_RBF_DKL_Matern.csv', index_col=0, header=[0, 1])
models = ['DKL_RBF', 'DKL_Matern', 'DKL_SM']

row = [0, 0, 1]
col = [2, 3, 3]

for l, model in enumerate(models):
    if any(df[(model, 'pred')].isna()):
        continue

        # plt.subplot(4, 5, geom+1)
    errors = np.array([])

    for i_init in df[('data', 'init')].unique():
        sub_df = df[(df[('data', 'geom')] == geom) & (df[('data', 'init')] == i_init)]
        X = np.array(sub_df['X'])
        pred = sub_df[(model, 'pred')]
        y = sub_df[('truth', 'y')]
        errors = np.append(errors, evaluate_1d(pred, y, X[:, 1]))
        # axarray[geom//5, geom % 5].plot(X[:, 1], pred, linewidth=0.8)

    sorting = errors.argsort()

    preds = []
    stds = []

    for k, i in enumerate(sorting[:5]):
        sub_df = df[(df[('data', 'geom')] == geom) & (df[('data', 'init')] == i)]
        X = np.array(sub_df['X'])
        preds.append(np.array(sub_df[(model, 'pred')]))
        stds.append(np.array(sub_df[(model, 'std')]))
        y = sub_df[('truth', 'y')]
    pred = np.mean(preds, axis=0)
    std = np.mean(stds, axis=0)
    axarray[row[l], col[l]].plot(X[:, 1], pred, c=c_palette[0], linewidth=0.8, alpha=0.6)
    axarray[row[l], col[l]].fill_between(X[:, 1], pred - 2 * std, pred + 2 * std,
                                              color='lightblue', alpha=0.5,
                                              label=r"$2\sigma$")

    std = (std / np.std(std)) * 3
    axarray[row[l], col[l]].plot(X[:, 1], std-(np.max(std)+60), linewidth=0.1, alpha=0.6, c='k')

    axarray[row[l], col[l]].plot(X[:, 1], y, c='#3D3E3F', linestyle=(0, (3, 1)), linewidth=0.2,
                                      label='true function', alpha=0.5)
    axarray[row[l], col[l]].set_title(models[l].replace('_', ' '))


f.supxlabel("Frequency [GHz]", x=0.545, y=0.04)
f.supylabel("Magnitude [dB]", x=0.03, y=0.54)
f.tight_layout()
plt.savefig(f"results/S(1,1)/figures/final_products/Fn1000_preds_with_std_g{geom}.pdf", bbox_inches='tight')
plt.show()


import os

import matplotlib.pyplot as plt
import numpy as np
import matplotlib

from data_handling.data_structure import Data
from Models.DKL.DKLModel import DKLModel
from Models.DKL.DKLKernels import *
from Models.Gaussian_Process_Regression.GPyR_Matern import GPyRModelMatern
from Models.Gaussian_Process_Regression.GPyR_Matern_SKI import GPyRModelMaternSKI
from Models.Gaussian_Process_Regression.GPyR_RBF import GPyRModelRBF
from Models.Gaussian_Process_Regression.GPyR_RBF_SKI import GPyRModelRBFSKI
from Models.Gaussian_Process_Regression.GPyR_SM_SKI import GPyRModelSMSKI
from Utils import *
from matplotlib import font_manager as fm
if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

matplotlib.rc('xtick', labelsize=5)
matplotlib.rc('ytick', labelsize=5)

fe = fm.FontEntry(
    fname='C:\\Users\\krism\\AppData\\Local\\Microsoft\\Windows\\Fonts\\CharterBT-Roman.otf',
    name='Bitstream Charter')
fm.fontManager.ttflist.insert(0, fe)
plt.rcParams['font.family'] = 'Bitstream Charter'

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 12
plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
# plt.rc('legend', titlesize=MEDIUM_SIZE)    # legend fontsize
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

# Load data
filepath = '../Data/'
filename = 'dielectric.csv'


D = Data()
D.load(os.path.join(filepath, filename))
(Sr, Sc), (Fn, Gn) = D.dims()
X, y, geoms = D.S((1, 1), geom_index=None)
X = np.delete(X, np.arange(0, X.shape[0], 1001), axis=0)
y = np.delete(y, np.arange(0, y.shape[0], 1001), axis=0)
Fn -= 1
data_is_coarse = False

if data_is_coarse:
    every_nth = 10
    X, y = X[::every_nth], y[::every_nth]
    Fn = Fn // every_nth


D.load(os.path.join(filepath, 'MIDAS_dielectric_output_1geom_fine.csv'))
X_fine, y_fine, _ = D.S((1, 1), geom_index=None)
f = np.array([False if i % 10 else True for i in range(5000) for j in range(500)])
X_fine1 = X_fine[f][::]
y_fine1 = y_fine[f][::]

df = pd.read_csv(f'results/S(1,1)/n1_Fn1000_RBF_SKI_Matern_SKI_SM_SKI.csv', index_col=0, header=[0, 1])

geom = 6
filt = X[:, 0] == geoms[geom]
X_train, y_train = X[~filt], y[~filt]
X_test, y_test = X[filt], y[filt]

# Heatmap points
x1 = np.unique(X_fine1[:, 0])
y1 = np.unique(X_fine1[:, 1])

xy = np.asarray([[[xi, yi] for yi in y1] for xi in x1])
xg, yg = np.meshgrid(y1, x1)

f, axarray = plt.subplots(1, 4, sharex="col", sharey="row", figsize=(8, 3))

# True function
axarray[0].pcolormesh(xg, yg, y_fine1.reshape(xg.shape)[:-1, :-1], cmap='RdBu', vmin=-60, vmax=0, rasterized=True)
axarray[0].axhline(geoms[geom], color='#FBFCFC', linestyle=(0, (2, 1)), linewidth=0.8)
axarray[0].set_title('Data')

# Scrub models
n_training_iters = 100
# RBF
def create_rbf_model():
    RBF_noise = 0.5
    RBF_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    RBF_likelihood.noise = RBF_noise
    RBF_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelRBF(likelihood=RBF_likelihood, training_iterations=n_training_iters)

# RBF SKI
def create_rbf_ski_model():
    RBFSKI_noise = 0.5
    RBFSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    RBFSKI_likelihood.noise = RBFSKI_noise
    RBFSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelRBFSKI(likelihood=RBFSKI_likelihood, training_iterations=n_training_iters)

# MATERN
def create_matern_model():
    MATERN_noise = 0.5
    MATERN_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    MATERN_likelihood.noise = MATERN_noise
    MATERN_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelMatern(likelihood=MATERN_likelihood, training_iterations=n_training_iters)

# MATERN SKI
def create_matern_ski_model():
    MATERNSKI_noise = 0.5
    MATERNSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    MATERNSKI_likelihood.noise = MATERNSKI_noise
    MATERNSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelMaternSKI(likelihood=MATERNSKI_likelihood, training_iterations=n_training_iters)

# SM SKI
def create_sm_ski_model():
    SMSKI_noise = 0.5
    SMSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    SMSKI_likelihood.noise = SMSKI_noise
    SMSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelSMSKI(likelihood=SMSKI_likelihood, training_iterations=n_training_iters)

model_names = ['RBF SKI', 'Matern SKI', 'SM SKI']
# model_names = ['SM SKI']
#
models = [create_rbf_ski_model(), create_matern_ski_model(), create_sm_ski_model()]
# models = [create_sm_ski_model()]

bounds = np.array([[min(X_fine[:, 0]), max(X_fine[:, 0])], [min(X_fine[:, 1]), max(X_fine[:, 1])]])
# SCRUB MODELS
for l, model in enumerate(models):
    errors = np.array([])
    for i_init in df[('data', 'init')].unique():
        sub_df = df[(df[('data', 'geom')] == geom) & (df[('data', 'init')] == i_init)]
        X_ = np.array(sub_df['X'])
        pred = sub_df[(model.name, 'pred')]
        y_ = sub_df[('truth', 'y')]
        errors = np.append(errors, evaluate_1d(pred, y_, X_[:, 1]))

    sorting = errors.argsort()

    z = np.zeros((5, *xy.shape[:-1]))
    for j, idx in enumerate(sorting[:5]):
        model.load_model(X_train, y_train, bounds, f'results/S(1,1)/models/{model.name}/Fn{Fn}/g{geom}_n{0}.pth')
        for i, xyi in enumerate(xy):
            z[j][i] = model.predict(xyi, np.zeros(xyi.shape[0]))

    z = np.mean(z, axis=0)
    z_min, z_max = min(y_fine1.min(), z.min()), max(y_fine1.max(), z.max())
    zr = z[:-1, :-1]
    c = axarray[l+1].pcolormesh(xg, yg, zr, cmap='RdBu', vmin=-50, vmax=0, rasterized=True)
    axarray[l+1].axhline(geoms[geom], color='#FBFCFC', linestyle=(0, (2, 1)), linewidth=0.8)
    # set the limits of the plot to the limits of the data
    axarray[l+1].axis([xg.min(), xg.max(), yg.min(), yg.max()])
    axarray[l+1].set_title(model_names[l])
    print(model_names[l])


f.supxlabel("Frequency [GHz]", x=0.51, y=0.07)
f.supylabel("Magnitude [dB]", x=0.03, y=0.55)
plt.tight_layout()

f.subplots_adjust(right=0.91)
cbar_ax = f.add_axes([0.935, 0.258, 0.02, 0.615])
f.colorbar(c, cax=cbar_ax)
plt.savefig(f"results/S(1,1)/figures/final_products/Fn1000_preds_SKI_models_g{geom}_new_2.pdf", bbox_inches='tight', dpi=1000)
plt.show()

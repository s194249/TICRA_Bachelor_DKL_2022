import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.font_manager as fm
from Utils import *

matplotlib.rc('xtick', labelsize=5)
matplotlib.rc('ytick', labelsize=5)

fe = fm.FontEntry(
    fname='C:\\Users\\krism\\AppData\\Local\\Microsoft\\Windows\\Fonts\\CharterBT-Roman.otf',
    name='Bitstream Charter')
fm.fontManager.ttflist.insert(0, fe)
plt.rcParams['font.family'] = 'Bitstream Charter'

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 16
plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
# plt.rc('legend', titlesize=MEDIUM_SIZE)    # legend fontsize
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

df = pd.read_csv('results/S(1,1)/n10_Fn1000_DKL_SM_DKL_RBF_DKL_Matern.csv', index_col=0, header=[0, 1])
c_palette = ['#EC7063', '#3498DB', '#9B59B6', '#2ECC71', '#F1C40F']
# c_palette.reverse()
to_mean = False

models = [col[0] for col in df.columns if col[1] == 'pred']

error_df = pd.DataFrame(None, columns=['geom'] + ['init'] + models, index=range(100))

error_df['geom'] = np.repeat(np.arange(20), 5)
error_df['init'] = np.tile(np.arange(5), 20)

for model in models:
    if any(df[(model, 'pred')].isna()):
        continue

    f, axarray = plt.subplots(4, 5, sharex="col", sharey="row")
    f.suptitle(f'{model.replace("_", " ")}')
    total_errors = []
    for geom in df[('data', 'geom')].unique():
        # plt.subplot(4, 5, geom+1)
        errors = np.array([])

        for i_init in df[('data', 'init')].unique():
            sub_df = df[(df[('data', 'geom')] == geom) & (df[('data', 'init')] == i_init)]
            X = np.array(sub_df['X'])
            pred = sub_df[(model, 'pred')]
            y = sub_df[('truth', 'y')]
            errors = np.append(errors, evaluate_1d(pred, y, X[:, 1], metric='WAPE'))

        sorting = errors.argsort()

        if to_mean:
            preds = []
            stds = []

            for k, i in enumerate(sorting[:5]):
                sub_df = df[(df[('data', 'geom')] == geom) & (df[('data', 'init')] == i)]
                X = np.array(sub_df['X'])
                preds.append(np.array(sub_df[(model, 'pred')]))
                stds.append(np.array(sub_df[(model, 'std')]))
                y = sub_df[('truth', 'y')]
                total_errors.append(errors[i])
            pred = np.mean(preds, axis=0)
            std = np.mean(stds, axis=0)
            axarray[geom // 5, geom % 5].plot(X[:, 1], pred, c=c_palette[0], linewidth=0.8, alpha=0.6)
            axarray[geom // 5, geom % 5].fill_between(X[:, 1], pred - 2 * std, pred + 2 * std,
                                                      color='lightblue', alpha=0.5,
                                                      label=r"$2\sigma$")
            std = (std / np.std(std)) * 3
            axarray[geom // 5, geom % 5].plot(X[:, 1], std-(np.max(std)+50), linewidth=0.1, alpha=0.6, c='k')
        else:
            for k, i in enumerate(sorting[:5]):
                sub_df = df[(df[('data', 'geom')] == geom) & (df[('data', 'init')] == i)]
                X = np.array(sub_df['X'])
                pred = sub_df[(model, 'pred')]
                std = sub_df[(model, 'std')]
                y = sub_df[('truth', 'y')]
                axarray[geom // 5, geom % 5].plot(X[:, 1], pred, c=c_palette[k], linewidth=0.8, alpha=0.6)
                total_errors.append(errors[i])
        axarray[geom // 5, geom % 5].plot(X[:, 1], y, c='#3D3E3F', linestyle=(0, (3, 1)), linewidth=0.2,
                                          label='true function', alpha=0.5)

    error_df[model] = total_errors

    f.supxlabel("Frequency [GHz]", fontsize=10)
    f.supylabel("Magnitude [dB]", fontsize=10)
    f.tight_layout()
    # plt.savefig(f"results/S(1,1)/figures/n10_Fn100_{model}_mean{to_mean}_with_std.pdf")
    plt.show()
#error_df = error_df.groupby(["geom"]).mean().iloc[:, 1:]

print(f'Mean:\n{error_df.groupby(["geom"]).mean().iloc[:, 1:].mean()}')
print(f'STD:\n{error_df.groupby(["geom"]).mean().iloc[:, 1:].std()}')

import os
import time

import pandas as pd

from data_handling.data_structure import Data
from Models.DKL.DKLModel import DKLModel
from Models.DKL.DKLKernels import *
from Models.Gaussian_Process_Regression.GPyR_Matern import GPyRModelMatern
from Models.Gaussian_Process_Regression.GPyR_Matern_SKI import GPyRModelMaternSKI
from Models.Gaussian_Process_Regression.GPyR_RBF import GPyRModelRBF
from Models.Gaussian_Process_Regression.GPyR_RBF_SKI import GPyRModelRBFSKI
from Models.Gaussian_Process_Regression.GPyR_SM_SKI import GPyRModelSMSKI
from Utils import *


if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

# Load data
filepath = '../Data/'
filename = 'dielectric.csv'
D = Data()
D.load(os.path.join(filepath, filename))
(Sr, Sc), (Fn, Gn) = D.dims()
X, y, geom = D.S((1, 1), geom_index=None)
X = np.delete(X, np.arange(0, X.shape[0], 1001), axis=0)
y = np.delete(y, np.arange(0, y.shape[0], 1001), axis=0)
Fn -= 1

data_is_coarse = True

if data_is_coarse:
    every_nth = 10
    X, y = X[::every_nth], y[::every_nth]
    Fn = Fn // every_nth


### Define models ###
# DKL SM
dkl_training_iterations = 100
def create_dkl_SM_model():
    DKL_SM_noise = 0.5
    DKL_SM_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_SM_likelihood.noise = DKL_SM_noise
    DKL_SM_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_SM_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=SpectralMixtureGPModel,
                    training_iterations=dkl_training_iterations)

# DKL RBF
def create_dkl_RBF_model():
    DKL_RBF_noise = 0.5
    DKL_RBF_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_RBF_likelihood.noise = DKL_RBF_noise
    DKL_RBF_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_RBF_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=RBFGPModel,
                    training_iterations=dkl_training_iterations)

# DKL Matern
def create_dkl_Matern_model():
    DKL_MATERN_noise = 0.5
    DKL_MATERN_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_MATERN_likelihood.noise = DKL_MATERN_noise
    DKL_MATERN_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_MATERN_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=MaternGPModel,
                    training_iterations=dkl_training_iterations)

n_training_iters = 100
# RBF
def create_rbf_model():
    RBF_noise = 0.5
    RBF_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    RBF_likelihood.noise = RBF_noise
    RBF_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelRBF(likelihood=RBF_likelihood, training_iterations=n_training_iters)

# RBF SKI
def create_rbf_ski_model():
    RBFSKI_noise = 0.5
    RBFSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    RBFSKI_likelihood.noise = RBFSKI_noise
    RBFSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelRBFSKI(likelihood=RBFSKI_likelihood, training_iterations=n_training_iters)

# MATERN
def create_matern_model():
    MATERN_noise = 0.5
    MATERN_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    MATERN_likelihood.noise = MATERN_noise
    MATERN_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelMatern(likelihood=MATERN_likelihood, training_iterations=n_training_iters)

# MATERN SKI
def create_matern_ski_model():
    MATERNSKI_noise = 0.5
    MATERNSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    MATERNSKI_likelihood.noise = MATERNSKI_noise
    MATERNSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelMaternSKI(likelihood=MATERNSKI_likelihood, training_iterations=n_training_iters)

# SM SKI
def create_sm_ski_model():
    SMSKI_noise = 0.5
    SMSKI_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    SMSKI_likelihood.noise = SMSKI_noise
    SMSKI_likelihood.noise_covar.raw_noise.requires_grad_(False)
    return GPyRModelSMSKI(likelihood=SMSKI_likelihood, training_iterations=n_training_iters)


models = [create_rbf_model, create_matern_model, create_rbf_ski_model,  create_matern_ski_model,
          create_sm_ski_model, create_dkl_RBF_model, create_dkl_Matern_model, create_dkl_SM_model]
model_names = ['RBF', 'Matern', 'RBF_SKI', 'Matern_SKI', 'SM_SKI', 'DKL_RBF', 'DKL_Matern', 'DKL_SM']

# Initialise dataframe
df = pd.DataFrame(None, columns=model_names, index=range(20))

for model_gen, model_name in zip(models, model_names):
    # Initialise arrays for results
    model_training_times = []
    for geom_index in range(Gn):
        # Pick out one value of the geometric parameter
        filter = X[:, 0] == geom[geom_index]
        X_train, y_train = X[~filter], y[~filter]
        X_test, y_test = X[filter], y[filter]

        time.sleep(0.01)
        torch.manual_seed(42 + geom_index*42)
        model = model_gen()
        # Fit model and do predictions
        t = time.time()
        model.fit(X_train, y_train, grid=200)
        model_training_times.append(time.time() - t)

    df[model_name] = model_training_times

    print(f'Average training for {model_name}: {np.mean(model_training_times)}')


df.to_csv(f'results/S(1,1)/training_times/Fn{Fn}_training_times.csv', index=True)

import os

from data_handling.data_structure import Data
from Models.DKL.DKLModel import DKLModel
from Models.DKL.DKLKernels import *
from Models.Gaussian_Process_Regression.GPyR_Matern import GPyRModelMatern
from Utils import *

torch.manual_seed(11)
if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

filepath = '../Data/'
filename = 'dielectric.csv'

D = Data()
D.load(os.path.join(filepath, filename))
(Sr, Sc), (Fn, Gn) = D.dims()

X, y, geom = D.S((1, 1), geom_index=None)

noises = [None] #[0.01, 0.05, 0.1, 0.2, 0.3, 0.5, 0.75, 1]

geom_col = np.repeat(np.arange(Gn), Fn)

header = [(noise, v) for noise in noises for v in ('pred', 'std')]
header = pd.MultiIndex.from_tuples(header)

df = pd.DataFrame(None, index=geom_col, columns=header)
training_iterations = 100

for noise in noises:

    likelihood = gpytorch.likelihoods.GaussianLikelihood()
    model = GPyRModelMatern(likelihood=likelihood, training_iterations=training_iterations)
    model_name = 'Matern'

    all_geom_preds = np.zeros(Fn * Gn)
    all_geom_stds = np.zeros(Fn * Gn)

    print('#'*50 + f'Noise: {noise}')
    print(df)

    fit_noise = []
    for geom_index in range(Gn):
        filter = X[:, 0] == geom[geom_index]

        X_train, y_train = X[~filter], y[~filter]
        X_test, y_test = X[filter], y[filter]

        model.fit(X_train, y_train)

        pred, std = model.predict(X_test, y_test, return_std=True)

        all_geom_preds[geom_index * Fn: (1 + geom_index) * Fn] = pred
        all_geom_stds[geom_index * Fn: (1 + geom_index) * Fn] = std

        error = evaluate_1d(pred, y_test, X_test[:, 1])
        fit_noise.append(likelihood.noise.item())
        print(f'Geom {geom_index} done with error {error} using noise {likelihood.noise.item()}\n'+'#'*50)

    df[(noise, 'pred')] = all_geom_preds
    df[(noise, 'std')] = all_geom_stds

#noise_range = (round(min(fit_noise), 2), round(max(fit_noise), 2))
#df.columns = pd.MultiIndex.from_tuples([(noise_range, 'pred'), (noise_range, 'std')])

df.to_csv(
    f"Results/S(1,1)/parameter/Matern_fit_noise_tuning.csv",
    index=True)



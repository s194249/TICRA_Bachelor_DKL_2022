import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.font_manager as fm

from data_handling.data_structure import Data
from Utils import *

# Define path to csv file
filepath = '../Data/'
filename = 'dielectric.csv'

fe = fm.FontEntry(
    fname='C:\\Users\\krist\\AppData\\Local\\Microsoft\\Windows\\Fonts\\CharterBT-Roman.otf',
    name='Bitstream Charter')
fm.fontManager.ttflist.insert(0, fe)
plt.rcParams['font.family'] = 'Bitstream Charter'

SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 16
plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
#plt.rc('legend', titlesize=MEDIUM_SIZE)    # legend fontsize
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title



D = Data()
D.load(os.path.join(filepath, filename))
(Sr, Sc), (Fn, Gn) = D.dims()

model_names = ['DKL (SM)', 'Matern']

c_palette = ['#EC7063', '#3498DB', '#9B59B6',
             '#2ECC71', '#F1C40F', '#F48FB1',
             '#B9770E', '#16A085', '#A6ACAF']

plt.figure(figsize=(10, 7))
for n, model_name in enumerate(model_names):
    df = pd.read_csv(f'Results/S(1,1)/parameter/{model_name.split(" ")[0]}_noise_tuning.csv', index_col=0, header=[0, 1])

    noises = list(df.columns.get_level_values(0).unique())
    errors = [[] for _ in range(len(noises))]
    geoms = []

    for geom_index in range(Gn):
        X, y, geom = D.S((1, 1), geom_index=geom_index)
        pred = df[df.index.values == geom_index]
        geoms.append(geom)

        g = 9

        if geom_index == g:
            print(geom)
            plt.subplot(2, 2, n + 3)
        for i, noise in enumerate(noises):
            col = (noise, 'pred')
            error = evaluate_1d(pred[col], y, X, metric='tue')
            errors[i].append(error)
            if geom_index == g:
                plt.plot(X, pred[col], label=noise, c=c_palette[i], alpha=0.7)
        if geom_index == g:
            print(geom)
            plt.plot(X, y, '--k', linewidth=0.9, label='true function')

            plt.legend(title='Noise')
            #plt.title(f'Predictions for geometric value {geom:.2f}', loc='center')
            plt.xlabel('Frequency [GHz]')
            plt.ylabel('Magnitude [dB]')
            plt.xticks()
            plt.yticks()

    plt.subplot(2, 2, n+1)
    # flierprops={'markersize': 4, 'markeredgewidth': 0.3}
    box = plt.boxplot(errors, patch_artist=True)
    plt.xticks(range(1, len(noises)+1), noises)
    plt.ylabel('WAPE')
    plt.xlabel('Noise')
    plt.title(model_name, loc='center')

    for median, patch, flier, color in zip(box['medians'], box['boxes'], box['fliers'], c_palette):
        median.set_color('black')
        median.set_linestyle('--')
        median.set_linewidth(0.8)
        patch.set_facecolor(color)
        patch.set_linewidth(0.5)
        patch.set_alpha(0.7)
        flier.set_markerfacecolor(color)
        flier.set_markersize(5)
        flier.set_markeredgewidth(0.3)
        flier.set_alpha(0.7)

    for whisker, cap in zip(box['whiskers'], box['caps']):
        whisker.set_linewidth(0.5)
        cap.set_linewidth(0.5)

plt.tight_layout()
plt.savefig('Results/plots/noise_tuning.pdf')
plt.show()
import os

import matplotlib.pyplot as plt
import numpy as np
import matplotlib

from data_handling.data_structure import Data
from Models.DKL.DKLModel import DKLModel
from Models.DKL.DKLKernels import *
from Utils import *
from matplotlib import font_manager as fm
if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

matplotlib.rc('xtick', labelsize=5)
matplotlib.rc('ytick', labelsize=5)

fe = fm.FontEntry(
    fname='C:\\Users\\krism\\AppData\\Local\\Microsoft\\Windows\\Fonts\\CharterBT-Roman.otf',
    name='Bitstream Charter')
fm.fontManager.ttflist.insert(0, fe)
plt.rcParams['font.family'] = 'Bitstream Charter'

SMALL_SIZE = 8
MEDIUM_SIZE = 12
BIGGER_SIZE = 16
plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
# plt.rc('legend', titlesize=MEDIUM_SIZE)    # legend fontsize
plt.rc('axes', titlesize=BIGGER_SIZE)  # fontsize of the figure title

# Load data
filepath = 'Data/'
filename = 'dielectric.csv'


D = Data()
D.load(os.path.join(filepath, filename))
(Sr, Sc), (Fn, Gn) = D.dims()
X, y, geoms = D.S((1, 1), geom_index=None)
X = np.delete(X, np.arange(0, X.shape[0], 1001), axis=0)
y = np.delete(y, np.arange(0, y.shape[0], 1001), axis=0)
Fn -= 1
data_is_coarse = False

if data_is_coarse:
    every_nth = 10
    X, y = X[::every_nth], y[::every_nth]
    Fn = Fn // every_nth


D.load(os.path.join(filepath, 'MIDAS_dielectric_output_1geom_fine.csv'))
X_fine, y_fine, _ = D.S((1, 1), geom_index=None)
f = np.array([False if i % 10 else True for i in range(5000) for j in range(500)])
X_fine1 = X_fine[f]
y_fine1 = y_fine[f]

f = np.array([False if i % 50 else True for i in range(5000) for j in range(500)])
X_fine2 = X_fine[f][::5]
y_fine2 = y_fine[f][::5]
order = np.argsort(y_fine2)

# DKL
dkl_training_iterations = 100

# DKP SM
def create_dkl_SM_model():
    DKL_SM_noise = 0.5
    DKL_SM_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_SM_likelihood.noise = DKL_SM_noise
    DKL_SM_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_SM_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=SpectralMixtureGPModel,
                    training_iterations=dkl_training_iterations)

# DKL RBF
def create_dkl_RBF_model():
    DKL_RBF_noise = 0.5
    DKL_RBF_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_RBF_likelihood.noise = DKL_RBF_noise
    DKL_RBF_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_RBF_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=RBFGPModel,
                    training_iterations=dkl_training_iterations)

# DKL Matern
def create_dkl_Matern_model():
    DKL_MATERN_noise = 0.5
    DKL_MATERN_likelihood = gpytorch.likelihoods.GaussianLikelihood()
    DKL_MATERN_likelihood.noise = DKL_MATERN_noise
    DKL_MATERN_likelihood.noise_covar.raw_noise.requires_grad_(False)
    feature_extractor = LargeFeatureExtractor
    return DKLModel(likelihood=DKL_MATERN_likelihood,
                    feature_extractor=feature_extractor,
                    GP_model=MaternGPModel,
                    training_iterations=dkl_training_iterations)

models = [create_dkl_SM_model(),
          create_dkl_Matern_model(),
          create_dkl_RBF_model()]

model_names = ['DKL (SM)', 'DKL (Matern)', 'DKL (RBF)']

df = pd.read_csv(f'2d/results/S(1,1)/n10_Fn{Fn}_DKL_SM_DKL_RBF_DKL_Matern.csv', index_col=0, header=[0, 1])

# Heatmap points
x1 = np.unique(X_fine1[:, 0])
y1 = np.unique(X_fine1[:, 1])

xy = np.asarray([[[xi, yi] for yi in y1] for xi in x1])
xg, yg = np.meshgrid(y1, x1)

# Feature mapping points
x1f = np.unique(X_fine2[:, 0])
y1f = np.unique(X_fine2[:, 1])

xgf, ygf = np.meshgrid(y1f, x1f)

ming, maxg = min(x1f), max(x1f)
minf, maxf = min(y1f), max(y1f)

zf = np.asarray(
    [[180 / 255, (gf[0] - ming) / (maxg - ming) * 250 / 255, (gf[1] - minf) / (maxf - minf) * 250 / 255] for gf
     in X_fine2.reshape(-1, 2)])

for geom in [7]:#df[('data', 'geom')].unique():
    print(f'Plotting for geom: {geom+1}/{Gn}')
    filt = X[:, 0] == geoms[geom]
    X_train, y_train = X[~filt], y[~filt]
    X_test, y_test = X[filt], y[filt]

    best = []
    worst = []
    for model in models:
        errors = np.array([])
        for i_init in df[('data', 'init')].unique():
            sub_df = df[(df[('data', 'geom')] == geom) & (df[('data', 'init')] == i_init)]
            X_ = np.array(sub_df['X'])
            pred = sub_df[(model.name, 'pred')]
            y_ = sub_df[('truth', 'y')]
            errors = np.append(errors, evaluate_1d(pred, y_, X_[:, 1]))

        sorting = errors.argsort()
        best.append((sorting[0], errors[sorting[0]]))
        worst.append((sorting[-1], errors[sorting[-1]]))

    for L, L_name in zip([best, worst], ['Best', 'Worst']):
        print(f'\t> {L_name}')
        print(f'\t\t- Heatmaps')
        z = np.zeros((len(models), *xy.shape[:-1]))
        m = 0
        for model, (k, err) in zip(models, L):
            print(f'\t\t\t+ {model.name}')
            model.load_model(X_train, y_train, f'2d/results/S(1,1)/models/{model.name}/Fn{Fn}/g{geom}_n{k}.pth')

            for i, xyi in enumerate(xy):
                if not (i+1) % 50:
                    print(f'\t\t\t\t* Row {i+1}/{len(xy)}')
                z[m][i] = model.predict(xyi, np.zeros(xyi.shape[0]))
            m += 1

        fig, axes = plt.subplots(3, len(L)+1, figsize=(12, 9), dpi=200)
        z_min, z_max = min(y_fine1.min(), z.min()), max(y_fine1.max(), z.max())
        axes[0, 0].pcolormesh(xg, yg, y_fine1.reshape(xg.shape)[:-1, :-1], cmap='RdBu',
                              vmin=z_min, vmax=z_max, rasterized=True)
        axes[0, 0].set_title('Data')
        axes[0, 0].set_xlabel('Frequency [GHz]')
        axes[0, 0].set_ylabel('Geometric paramter $l_2$')
        for mz, model, model_name, ax in zip(z, models, model_names, axes[0, 1:]):
            zr = mz[:-1, :-1]
            c = ax.pcolormesh(xg, yg, zr, cmap='RdBu', vmin=z_min, vmax=z_max, rasterized=True)
            ax.axis([xg.min(), xg.max(), yg.min(), yg.max()])
            ax.set_title(model_name)
            ax.set_xlabel('Frequency [GHz]')

        print('\t\t- Feature mappings')
        # Plot feature mapping

        axes[1, 0].set_ylabel('Geometric paramter $l_2$')
        axes[1, 0].scatter(X_fine2[:, 1], X_fine2[:, 0], c=zf, s=1.65, rasterized=True)
        axes[1, 0].set_xlabel('Frequency [GHz]')
        axes[1, 0].set_xlim(minf, maxf)
        axes[1, 0].set_ylim(ming, maxg)

        c2 = axes[2, 0].scatter(X_fine2[:, 1], X_fine2[:, 0], c=y_fine2, s=1.65, cmap='RdBu', rasterized=True)
        axes[2, 0].set_ylabel('Geometric paramter $l_2$')
        axes[2, 0].set_xlabel('Frequency [GHz]')
        axes[2, 0].set_xlim(minf, maxf)
        axes[2, 0].set_ylim(ming, maxg)

        for m, (model, model_name) in enumerate(zip(models, model_names)):
            if model.name == 'DKL_Matern':
                lengthscale = model.model.covar_module.base_kernel.base_kernel.lengthscale.cpu().detach().numpy()
                fig.suptitle(f'1: {lengthscale[0, 1]}, 0: {lengthscale[0, 0]}')

            print(f'\t\t\t+ {model.name}')
            # Map the points through nnet
            sxy, _ = model.standardise(X_fine2, y_fine2)
            xy2 = model.model.scale_to_bounds(
                model.model.feature_extractor(torch.tensor(sxy).float().to(device))).cpu().detach().numpy()
            offset = 0.025
            minx, maxx = xy2[:, 1].min() - offset, xy2[:, 1].max() + offset
            miny, maxy = xy2[:, 0].min() - offset, xy2[:, 0].max() + offset

            # axes[1, m+1].set_title(model_name)
            axes[1, m+1].scatter(xy2[:, 1], xy2[:, 0], c=zf, s=1.65, rasterized=True)
            axes[1, m+1].set_xlim(minx, maxx)
            axes[1, m+1].set_ylim(miny, maxy)

            axes[2, m+1].scatter(xy2[:, 1], xy2[:, 0], c=y_fine2, s=1.65, cmap='RdBu', rasterized=True)
            axes[2, m+1].set_xlim(minx, maxx)
            axes[2, m+1].set_ylim(miny, maxy)

        plt.tight_layout()

        fig.subplots_adjust(right=0.94)

        cbar_ax = fig.add_axes([0.9535, 0.700, 0.012, 0.252])
        fig.colorbar(c, cax=cbar_ax)

        cbar_ax2 = fig.add_axes([0.9535, 0.066, 0.012, 0.252])
        fig.colorbar(c2, cax=cbar_ax2)

        #plt.savefig(f'2d/results/S(1,1)/figures/feature_extractor/{L_name}_g{geom}_rasterized.pdf', dpi=800)
        plt.show()







